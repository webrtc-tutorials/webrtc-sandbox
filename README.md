# WebRTC Sandbox

Sandbox Project for WebRTC samples and experiments

## Default aspect ratios

### For the default 16:9 aspect ratio

- 2160p: 3840x2160
- 1440p: 2560x1440
- 1080p: 1920x1080
- 720p: 1280x720
- 480p: 854x480
- 360p: 640x360
- 240p: 426x240

## Janus Gateway API

### The server root

_/api/rtc-http_

You can only contact the server root when you want to create a new session with the server.

Error Response:

```JSON
{
    "janus": "error",
    "transaction": "a1b2c3d4"
    "error": {
        "code" : 458,
        "reason" : "Could not find session 12345678"
    }
}
```

**Create Janus session**

Request:

```JSON
{
    "janus": "create",
    "transaction": "<random alphanumeric string>"
}
```

Response:

```JSON
{
    "janus": "success",
    "transaction": "<same as the request>",
    "data": {
        "id": "<unique integer session ID>"
    }
}
```

### The Session endpoint

_/api/rtc-http/12345678_

This endpoint can be used in two different ways:

1. using a parameter-less GET request to the endpoint, you'll issue a long-poll request to be notified about events and incoming messages from this session;
2. using a POST request to send JSON messages, you'll interact with the session itself.

**Attach plugin**

Request:

```JSON
{
    "janus": "attach",
    "plugin": "<the plugin's unique package name>",
    "transaction": "<random string>"
}
```

Response:

```JSON
{
    "janus": "success",
    "transaction": "<same as the request>",
    "data": {
        "id": "<unique integer plugin handle ID>"
    }
}
```

**Destroy session**

Request:

```JSON
{
    "janus": "destroy",
    "transaction": "<random string>"
}
```

### The Plugin Handle endpoint

_/api/rtc-http/12345678/98765432_

You can use this plugin handle for everything that is related to the communication with a plugin, that is, send the plugin a message, negotiate a WebRTC connection to attach to the plugin, and so on.

To send a plugin a message/request, you need to POST the handle endpoint a janus "message" JSON payload. The body field will have to contain a plugin-specific JSON payload. In case the message also needs to convey WebRTC-related negotiation information, a jsep field containing the JSON-ified version of the JSEP object can be attached as well.

**Mute Video message**

```JSON
{
    "janus": "message",
    "transaction": "<random string>",
    "body": {
        "audio": false
    }
}
```

**Mute Video with negotiation information message**

```JSON
{
    "janus": "message",
    "transaction": "<random string>",
    "body": {
        "audio": false
    },
    "jsep": {
        "type": "offer",
        "sdp": "v=0\r\no=[..more sdp stuff..]"
    }
}
```

**Detach Plugin Handle message**

```JSON
{
    "janus": "detach",
    "transaction": "<random string>"
}
```

**Keep Plugin Handle but hangup peer connection message**

```JSON
{
    "janus": "hangup",
    "transaction": "<random string>"
}
```

### Video Room API

**Create Video Room**

Request:

```JSON
{
    "janus": "message",
    "transaction": "<random string>",
    "body": {
        "request": "create",
        "room": <unique numeric ID, optional, chosen by plugin if missing>,
        "permanent": <true|false, whether the room should be saved in the config file, default=false>,
        "description": "<pretty name of the room, optional>",
        "secret": "<password required to edit/destroy the room, optional>",
        "pin": "<password required to join the room, optional>",
        "is_private": <true|false, whether the room should appear in a list request>,
        "allowed": [array of string tokens users can use to join this room, optional]
    }
}
```

Response:

```JSON
{
    "videoroom": "created",
    "room": "<unique numeric ID>",
    "permanent": <true if saved to config file, false if not>
}
```

**Destroy Video Room**

Request:

```JSON
{
    "janus": "message",
    "transaction": "<random string>",
    "body": {
        "request": "destroy",
        "room": <unique numeric ID of the room to destroy>,
        "secret": "<room secret, mandatory if configured>",
        "permanent": <true|false, whether the room should be also removed from the config file, default=false>
    }
}
```

Response:

```JSON
{
    "videoroom": "destroyed",
    "room": <unique numeric ID of the destroyed room>
}
```

**List Video Rooms**

Request:

```JSON
{
    "janus": "message",
    "transaction": "<random string>",
    "body": {
        "request": "list"
    }
}
```

Response:

```JSON
{
    "videoroom": "success",
    "rooms": [ // Array of room objects
        { // Room #1
            "room": <unique numeric ID>,
            "description": "<Name of the room>",
            "pin_required": <true|false, whether a PIN is required to join this room>,
            "max_publishers": <how many publishers can actually publish via WebRTC at the same time>,
            "bitrate": <bitrate cap that should be forced (via REMB) on all publishers by default>,
            "bitrate_cap": <true|false, whether the above cap should act as a limit to dynamic bitrate changes by publishers>,
            "fir_freq": <how often a keyframe request is sent via PLI/FIR to active publishers>,
            "audiocodec": "<comma separated list of allowed audio codecs>",
            "videocodec": "<comma separated list of allowed video codecs>",
            "record": <true|false, whether the room is being recorded>,
            "record_dir": "<if recording, the path where the .mjr files are being saved>",
            "num_participants": <count of the participants (publishers, active or not; not subscribers)>
        },
        // Other rooms
    ]
}
```
