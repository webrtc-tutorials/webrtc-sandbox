import { createLogger } from 'redux-logger';
import { applyMiddleware, createStore, Reducer } from "redux";

const DEBUG = (process.env.NODE_ENV !== "production");

const logger = createLogger();

const devMiddleware = [logger];

const middleware = (DEBUG) ? [...devMiddleware] : [];

function makeStore(rootReducer: Reducer<any>) {
    return createStore(rootReducer, applyMiddleware(...middleware));
}

export { makeStore };
