import * as React from 'react';
import { withRouter } from 'react-router-dom';
import { Breadcrumb } from 'antd';
import styled from 'styled-components';

const BreadCrumb = styled(Breadcrumb)`
    margin: 16px 0;
`;

const Breadcrumbs = (props: {location: any}) => {
    const parts = props.location.pathname.split("/").filter((item: any) => item.length > 0);

    return (
        <BreadCrumb>
            {parts.map((item: any, index: number) =>
                <Breadcrumb.Item key={index}>{item}</Breadcrumb.Item>
            )}
        </BreadCrumb>
    );
};

export default withRouter(Breadcrumbs);