import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';
import styled from 'styled-components';

import * as routes from '../routes';

const LayoutSider = styled(Layout.Sider)`
    overflow: auto;
    height: 100vh;
    position: fixed;
    left: 0;
`;

const Sidebar = (props: { location: any }) => {
    const parts = props.location.pathname.split("/").filter((item: any) => item.length > 0);

    return (
        <LayoutSider width="250px">
            <Menu
                theme="dark"
                mode="inline"
                defaultSelectedKeys={[parts[1] || routes.USER_MEDIA_CONSTRAINTS]}
                defaultOpenKeys={[parts[0] || routes.DEMOS]}
            >
                <Menu.SubMenu
                    key={routes.DEMOS}
                    title={<span><Icon type="code"/><span>Demos</span></span>}
                >
                    <Menu.Item key={routes.USER_MEDIA_CONSTRAINTS}>
                        <NavLink to={`/${routes.DEMOS}/${routes.USER_MEDIA_CONSTRAINTS}`}>User Media Constraints</NavLink>
                    </Menu.Item>

                    <Menu.Item key={routes.USER_MEDIA_BANDWIDTH}>
                        <NavLink to={`/${routes.DEMOS}/${routes.USER_MEDIA_BANDWIDTH}`}>User Media Bandwith</NavLink>
                    </Menu.Item>
                </Menu.SubMenu>

                <Menu.SubMenu
                    key={routes.WEBRTC}
                    title={<span><Icon type="code-sandbox" /><span>WebRTC</span></span>}
                >
                    <Menu.Item key={routes.SETTINGS}>
                        <NavLink to={`/${routes.WEBRTC}/${routes.SETTINGS}`}>Settings</NavLink>
                    </Menu.Item>

                    <Menu.Item key={routes.SIGNALING}>
                        <NavLink to={`/${routes.WEBRTC}/${routes.SIGNALING}`}>Signaling</NavLink>
                    </Menu.Item>

                    <Menu.Item key={routes.VIDEO_CALL_FLOW}>
                        <NavLink to={`/${routes.WEBRTC}/${routes.VIDEO_CALL_FLOW}`}>Video Call Flow</NavLink>
                    </Menu.Item>
                </Menu.SubMenu>

                <Menu.SubMenu
                    key={routes.JANUS}
                    title={<span><Icon type="rocket" /><span>Janus Gateway</span></span>}
                >
                    <Menu.Item key={routes.ROOMS_LIST}>
                        <NavLink to={`/${routes.JANUS}/${routes.ROOMS_LIST}`}>Janus Rooms</NavLink>
                    </Menu.Item>

                    <Menu.Item key={routes.VIDEO_ROOM}>
                        <NavLink to={`/${routes.JANUS}/${routes.VIDEO_ROOM}`}>Video Room</NavLink>
                    </Menu.Item>
                </Menu.SubMenu>
            </Menu>
        </LayoutSider>
    );
};

export default withRouter(Sidebar);
