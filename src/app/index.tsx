import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import styled from 'styled-components';

import Sidebar from './sidebar';
import Breadcrumbs from './breadcrumbs';

import Demos from '../modules/demos';
import WebRtc from '../modules/webrtc';
import Janus from '../modules/janus';

import * as routes from '../routes';

const LayoutContainer = styled(Layout)`
    min-height: 100vh;
`;

const LayoutMain = styled(Layout)`
    margin-left: 250px;
`;

const LayoutHeader = styled(Layout.Header)`
    background: #fff;
`;

const LayoutContent = styled(Layout.Content)`
    margin: 0 16px;
`;

const LayoutContentMain = styled.div`
    padding: 24px;
    background: #fff;
    min-height: 360px;
`;

const App = () => (
    <Router basename="/client" >
        <LayoutContainer>
            <Sidebar />

            <LayoutMain>
                <LayoutHeader>
                    <h1>WebRTC Sandbox</h1>
                </LayoutHeader>

                <LayoutContent>
                    <Breadcrumbs />

                    <LayoutContentMain>
                        <Switch>
                            <Route
                                exact={true}
                                path="/"
                                render={() =>
                                    <Redirect to={`/${routes.DEMOS}`} />
                                }
                            />
                            <Route path={`/${routes.DEMOS}`} component={Demos} />
                            <Route path={`/${routes.WEBRTC}`} component={WebRtc} />
                            <Route path={`/${routes.JANUS}`} component={Janus} />
                        </Switch>
                    </LayoutContentMain>
                </LayoutContent>
            </LayoutMain>
        </LayoutContainer>
    </Router>
);

export default App;
