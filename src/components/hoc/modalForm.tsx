import React from "react";
import { Modal, Form } from "antd";
import { FormComponentProps } from "antd/lib/form";

interface Props extends FormComponentProps {
    isOpen: boolean;
    title: string;
    entity?: any;
    onCancel: () => void;
    onSubmit: (data: any) => void;
}

const modalForm = (Component: any) => {
    return Form.create<Props>()(
        class extends React.Component<Props> {
            handleSubmit = () => {
                const {form, onSubmit} = this.props;

                form.validateFields((err, values) => {
                    if (err) {
                        return;
                    }

                    onSubmit(values);
                    form.resetFields();
                });
            };

            render() {
                const {
                    isOpen,
                    title,
                    entity,
                    onCancel,
                    form } = this.props;

                return (
                    <Modal
                        visible={isOpen}
                        title={title}
                        okText="Submit"
                        onCancel={onCancel}
                        onOk={this.handleSubmit}
                    >
                        <Component form={form} entity={entity} />
                    </Modal>
                );
            }
        }
    );
};

export default modalForm;
