import React from 'react';
import { Select, Spin } from 'antd';

interface Props {
    name: string;
    placeholder: string;
    options: Array<any>;
    value: any;
    fetching: boolean;
    width?: number;
    isLoading: boolean;
    onChange: (value: any) => void;
    onFetch: () => any;
}

interface State {
    query: string;
}

class ComboBox extends React.Component<Props, State> {

    static defaultProps = {
        placeholder: null,
        value: null,
        options: [],
        fetching: false
    };

    props: Props;

    readonly state: State = {
        query: ""
    };

    componentDidMount() {
        this.props.onFetch();
    }

    handleChange = (query: string) => {
        this.setState({ query });
    };

    handleSelect = (value: any) => {
        const {onChange} = this.props;
        onChange(value);
    };

    render() {
        const {
            placeholder,
            value,
            options,
            fetching,
            width,
        } = this.props;

        const {query} = this.state;

        let optionTags = options
            .filter(item => item.text.includes(query))
            .map(item =>
                <Select.Option key={item.text}>{item.text}</Select.Option>
            );

        if (optionTags.length === 0 && query.length > 0) {
            optionTags.push(<Select.Option key={query}>{query}</Select.Option>);
        }

        return (
            <Select
                showSearch={true}
                placeholder={placeholder}
                value={value}
                notFoundContent={fetching ? <Spin size="small"/> : null}
                optionFilterProp="children"
                onSearch={this.handleChange}
                onSelect={this.handleSelect}
                style={width ? {width: `${width}px`} : undefined}
            >
                {optionTags}
            </Select>
        );
    }
}

export default ComboBox;
