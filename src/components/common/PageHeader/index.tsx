import React from 'react';
import styled from 'styled-components';

const DEFAULT_SIZE = '24';

const Header = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    margin-bottom: 0.5em;
    padding-bottom: 0.5em;
    border-bottom: 1px solid rgba(34,36,38,.15);
`;

const HeaderContent = styled.div`
    flex: 2;
    padding-top: .3em;
    font-size: 18px;
    font-weight: 700;
    font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
`;

const HeaderActions = styled.div`
    flex: 1;
    display: flex;
    justify-content: flex-end;
`;

const PageHeader = (props: {children: JSX.Element | string, actions?: JSX.Element, size?: string}) => {
    let sizePx = (props.size === "h3") ? "18" : DEFAULT_SIZE;

    return (
        <Header>
            <HeaderContent style={{fontSize: `${sizePx}px`}}>{props.children}</HeaderContent>
            <HeaderActions>{props.actions}</HeaderActions>
        </Header>
    );
};

export default PageHeader;
