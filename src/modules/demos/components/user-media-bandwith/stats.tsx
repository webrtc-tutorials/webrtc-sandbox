import React from 'react';
import { Card, Statistic } from 'antd';
import styled from 'styled-components';

interface Props {
    stats?: RTCStatsReport;
}

interface State {
    lastResult?: RTCStatsReport;
    bitrate: number;
    numPackets: number;
}

const Container = styled.div`
    display: flex;
    justify-content: space-between;
`;

const StatsCard = styled(Card)`
    flex: 1 1 auto;
    margin: 5px;
`;

class Stats extends React.Component<Props, State> {

    props: Props;

    readonly state: State = {
        bitrate: 0.0,
        numPackets: 0
    }

    calcStats = () => {
        const { stats } = this.props;
        const { lastResult } = this.state;

        stats && stats.forEach(report => {
            if (report.type !== 'outbound-rtp' || report.isRemote) return;

            const now = report.timestamp;
            const bytes = report.bytesSent;
            const packets = report.packetsSent;

            if (lastResult && lastResult.has(report.id)) {
                // calculate bitrate
                const { bytesSent, packetsSent, timestamp } = lastResult.get(report.id);
                const bitrate = 8 * (bytes - bytesSent) / (now - timestamp);

                // calculate number of packets
                const numPackets = packets - packetsSent

                this.setState({ bitrate, numPackets });
            }
        });

        this.setState({ lastResult: stats });
    }

    componentDidUpdate(prevProps: Props) {
        if (prevProps.stats !== this.props.stats) {
            this.calcStats();
        }
    }

    render() {
        const { bitrate, numPackets } = this.state;

        console.log(`state: ${this.state.numPackets}`);

        return (
            <Container>
                <StatsCard>
                    <Statistic
                        title="Bitrate"
                        value={bitrate}
                        precision={2}
                        suffix="kbps"
                    />
                </StatsCard>

                <StatsCard>
                    <Statistic
                        title="Packets sent per second"
                        value={numPackets}
                        precision={0}
                    />
                </StatsCard>
            </Container>
        );
    }
}

export default Stats;
