import React from 'react';
import adapter from 'webrtc-adapter';
import { Radio, Button } from 'antd';
import styled from 'styled-components';

import PageHeader from '../../../../components/common/PageHeader';
import Stats from './stats';

interface State {
    bandwidth: number;
    isCalling: boolean;
    stats?: RTCStatsReport;
}

const FormContainer = styled.div`
    display: flex;
    justify-content: space-between;
`;

const FormActions = styled.div`
    display: flex;
    justify-content: space-between;
    width: 150px;
`;

const VideoContainer = styled.div`
    display: flex;
    justify-content: space-between;
`;

const Video = styled.video`
    margin: 5px;
    flex: 1 1 auto;
    border: 1px solid lightgrey;
    height: 300px;
`;

class UserMediaBandwith extends React.Component<{}, State> {

    readonly state: State = {
        bandwidth: 0,
        isCalling: false
    };

    private localVideoRef = React.createRef<HTMLVideoElement>();
    private remoteVideoRef = React.createRef<HTMLVideoElement>();

    private pc1: RTCPeerConnection;
    private pc2: RTCPeerConnection;
    private localStream;
    private timer;
    private lastResult;

    private onIceCandidate = (event: any) => {
        this.getOtherPc(this)
                .addIceCandidate(event.candidate)
                .then(this.onAddIceCandidateSuccess)
                .catch(this.onAddIceCandidateError);
    };

    private getOtherPc = (pc: any) => {
        return (pc === this.pc1) ? this.pc2 : this.pc1;
    };

    private onAddIceCandidateSuccess = () => {
        console.log('AddIceCandidate success.');
    };

    private onAddIceCandidateError = (error: any) => {
        console.log(`Failed to add ICE Candidate: ${error.toString()}`);
    };

    private onSetSessionDescriptionError = (error: any) => {
        console.log(`Failed to set session description: ${error.toString()}`);
    };

    private onCreateSessionDescriptionError = (error: any) => {
        console.log(`Failed to create session description: ${error.toString()}`);
    };

    private gotStream = (stream: any) => {
        console.log('in gotStream')
        console.log(this)

        this.localStream = stream;

        if (!this.localStream) return;
        this.localStream.getTracks().forEach(track => this.pc1.addTrack(track, this.localStream));

        const videoTag = this.localVideoRef.current;

        if (videoTag) {
            videoTag.srcObject = stream;
        }

        this.pc1.createOffer().then(this.gotDescription1, this.onCreateSessionDescriptionError);
    };

    private gotRemoteStream = (e: any) => {
        const videoTag = this.remoteVideoRef.current;
        const stream = e.streams[0];

        if (videoTag && videoTag.srcObject !== stream) {
            videoTag.srcObject = stream;
        }
    };

    private gotDescription1 = (desc: any) => {
        this.pc1.setLocalDescription(desc).then(
            () => {
                this.pc2.setRemoteDescription(desc)
                    .then(() => {
                        this.pc2.createAnswer().then(this.gotDescription2, this.onCreateSessionDescriptionError)
                    }, this.onSetSessionDescriptionError);
            }, this.onSetSessionDescriptionError
        );
    };

    private gotDescription2 = (desc: any) => {
        this.pc2.setLocalDescription(desc).then(
            () => {
                const { bandwidth } = this.state;
                let p;

                if (bandwidth) {
                    p = this.pc1.setRemoteDescription({
                        type: desc.type,
                        sdp: this.updateBandwidthRestriction(desc.sdp, bandwidth)
                    });
                } else {
                    p = this.pc1.setRemoteDescription(desc);
                }

                p.then(() => {}, this.onSetSessionDescriptionError);
            }, this.onSetSessionDescriptionError);
    };

    private updateBandwidthRestriction(sdp: string, bandwidth: number) {
        let modifier = 'AS';

        if (adapter.browserDetails.browser === 'firefox') {
            bandwidth = (bandwidth >>> 0) * 1000;
            modifier = 'TIAS';
        }

        if (sdp.indexOf('b=' + modifier + ':') === -1) {
            // insert b= after c= line.
            sdp = sdp.replace(/c=IN (.*)\r\n/, 'c=IN $1\r\nb=' + modifier + ':' + bandwidth + '\r\n');
        } else {
            sdp = sdp.replace(new RegExp('b=' + modifier + ':.*\r\n'), 'b=' + modifier + ':' + bandwidth + '\r\n');
        }

        return sdp;
    }

    private removeBandwidthRestriction(sdp: string) {
        return sdp.replace(/b=AS:.*\r\n/, '').replace(/b=TIAS:.*\r\n/, '');
    }

    private updateBandwithOnTheFly = () => {
        const { bandwidth } = this.state;

        // In Chrome, use RTCRtpSender.setParameters to change bandwidth without
        // (local) renegotiation. Note that this will be within the envelope of
        // the initial maximum bandwidth negotiated via SDP.
        // if ((adapter.browserDetails.browser === 'chrome' || (adapter.browserDetails.browser === 'firefox' && adapter.browserDetails.version >= 64))
        //         && 'RTCRtpSender' in window && 'setParameters' in window['RTCRtpSender'].prototype) {
        //     const sender = this.pc1.getSenders()[0];
        //     const parameters = sender.getParameters();

        //     if (!parameters.encodings) {
        //         parameters.encodings = [{}];
        //     }

        //     if (bandwith === 'unlimited') {
        //         delete parameters.encodings[0].maxBitrate;
        //     } else {
        //         parameters.encodings[0].maxBitrate = bandwith * 1000;
        //     }

        //     sender.setParameters(parameters).catch(e => console.error(e));

        //     return;
        // }

        // Fallback to the SDP munging with local renegotiation way of limiting
        // the bandwidth.
        this.pc1.createOffer()
                .then(offer => this.pc1.setLocalDescription(offer))
                .then(() => {
                    const rDesc = this.pc1.remoteDescription;

                    if (!rDesc) return;

                    const desc = {
                        type: rDesc.type,
                        sdp: (bandwidth === 0)
                            ? this.removeBandwidthRestriction(rDesc.sdp)
                            : this.updateBandwidthRestriction(rDesc.sdp, bandwidth)
                    };

                    console.log('Applying bandwidth restriction to setRemoteDescription:\n' + desc.sdp);

                    return this.pc1.setRemoteDescription(desc);
                })
                .catch(this.onSetSessionDescriptionError);
    };

    private onStatsUpdate = (report: RTCStatsReport) => {
        this.setState({ ...this.state, stats: report })
    };

    handleBandwithChange = (event: any) => {
        this.setState({ bandwidth: event.target.value }, () => this.updateBandwithOnTheFly());
    };

    handleCall = (event: any) => {
        this.setState({ isCalling: true });

        const servers = undefined;

        this.pc1 = new RTCPeerConnection(servers);
        this.pc1.onicecandidate = this.onIceCandidate;

        this.pc2 = new RTCPeerConnection(servers);
        this.pc2.onicecandidate = this.onIceCandidate;
        this.pc2.ontrack = this.gotRemoteStream;

        navigator.mediaDevices.getUserMedia({video: true, audio: true})
                .then(this.gotStream)
                .catch(e => console.log(`getUserMedia() error: ${e}`));

        // set timer for get stats
        // this.timer = setInterval(() => {
        //     const sender = this.pc1.getSenders()[0];
        //     sender.getStats().then(this.onStatsUpdate);
        // }, 1000)
    };

    handleHangup = (event: any) => {
        this.setState({ isCalling: false });

        clearInterval(this.timer);
        this.localStream.getTracks().forEach(track => track.stop());
        this.pc1.close();
        this.pc2.close();
    };

    render() {
        const { bandwidth, isCalling, stats } = this.state;

        return (
            <>
                <PageHeader>
                    Bandwidth manipulation
                </PageHeader>

                <FormContainer>
                    <Radio.Group buttonStyle="solid" value={bandwidth} disabled={!isCalling} onChange={this.handleBandwithChange}>
                        <Radio.Button value={0}>Unlimited</Radio.Button>
                        <Radio.Button value={2000}>2000 kbps</Radio.Button>
                        <Radio.Button value={1000}>1000 kbps</Radio.Button>
                        <Radio.Button value={500}>500 kbps</Radio.Button>
                        <Radio.Button value={250}>250 kbps</Radio.Button>
                        <Radio.Button value={125}>125 kbps</Radio.Button>
                    </Radio.Group>

                    <FormActions>
                        <Button type="primary" disabled={isCalling} onClick={this.handleCall}>Call</Button>
                        <Button type="danger" disabled={!isCalling} onClick={this.handleHangup}>HangUp</Button>
                    </FormActions>
                </FormContainer>

                <VideoContainer>
                    <Video ref={this.localVideoRef} autoPlay></Video>
                    <Video ref={this.remoteVideoRef} autoPlay></Video>
                </VideoContainer>

                <Stats stats={stats} />
            </>
        );
    }
}

export default UserMediaBandwith;
