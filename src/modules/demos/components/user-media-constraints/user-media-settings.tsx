import React from 'react';
import { connect } from 'react-redux';
import { Form, InputNumber, Input, Col } from "antd";
import { WrappedFormUtils } from "antd/lib/form/Form";

import modalForm from "../../../../components/hoc/modalForm";

import { editUserMediaConstraints } from '../../redux/actions/media-constraints';

interface Props {
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (formData: any) => void;
}

const FormComponent = modalForm((props: { form: WrappedFormUtils }) => {
    
    return (
        <Form>
            <Input.Group compact>
                <Col span={8}>
                    <Form.Item label="Width min">
                        {props.form.getFieldDecorator("minWidth", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={8}>
                    <Form.Item label="Width max">
                        {props.form.getFieldDecorator("maxWidth", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={8}>
                    <Form.Item label="Width ideal">
                        {props.form.getFieldDecorator("idealWidth", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>
            </Input.Group>

            <Input.Group compact>
                <Col span={8}>
                    <Form.Item label="Height min">
                        {props.form.getFieldDecorator("minHeight", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={8}>
                    <Form.Item label="Height max">
                        {props.form.getFieldDecorator("maxHeight", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={8}>
                    <Form.Item label="Height ideal">
                        {props.form.getFieldDecorator("idealHeight", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>
            </Input.Group>

            <Input.Group compact>
                <Col span={8}>
                    <Form.Item label="FRate min">
                        {props.form.getFieldDecorator("minFrameRate", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={8}>
                    <Form.Item label="FRate max">
                        {props.form.getFieldDecorator("maxFrameRate", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={8}>
                    <Form.Item label="FRate ideal">
                        {props.form.getFieldDecorator("idealFrameRate", {
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>
            </Input.Group>
        </Form>
    )
});

class UserMediaSettings extends React.Component<Props> {

    static defaultProps = {
        isOpen: false
    };

    props: Props;

    submitForm = (data: any) => {
        const { onSubmit, onClose } = this.props;

        onSubmit(data);
        onClose();
    };

    render() {
        const { isOpen, onClose } = this.props;

        return (
            <FormComponent
                isOpen={isOpen}
                title="Get User Media Settings"
                onCancel={onClose}
                onSubmit={this.submitForm}
            />
        );
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    onSubmit: (formData: any) => dispatch(editUserMediaConstraints(formData))
});

export default connect(null, mapDispatchToProps)(UserMediaSettings);
