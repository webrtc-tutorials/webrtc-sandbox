import React from 'react';
import { connect } from 'react-redux';
import { Button } from "antd";
import styled from 'styled-components';

import PageHeader from '../../../../components/common/PageHeader';
import UserMediaSettings from './user-media-settings';

import * as mediaUtils from '../../../common/util/get-user-media';
import { getMediaConstraints } from '../../redux/selectors/media-constraints';

type Constraints = {
    minWidth: number;
    maxWidth: number;
    idealWidth: number;
    minHeight: number;
    maxHeight: number;
    idealHeight: number;
    minFrameRate: number;
    maxFrameRate: number;
    idealFrameRate: number;
}

interface Props {
    constraints: Constraints;
}

interface State {
    isEdit: boolean;
    mediaStream?: MediaStream;
}

const Video = styled.video`
    margin-left: 5%;
    width: 90%;
    height: 70vh;
    border: 1px solid lightgrey;
`;

class UserMediaConstraints extends React.Component<Props, State> {

    static defaultProps = {
        constraints: {
            minWidth: 0,
            maxWidth: 0,
            idealWidth: 0,
            minHeight: 0,
            maxHeight: 0,
            idealHeight: 0,
            minFrameRate: 0,
            maxFrameRate: 0,
            idealFrameRate: 0
        }
    };

    props: Props;

    readonly state: State = {
        isEdit: false
    };

    private videoRef = React.createRef<HTMLVideoElement>();

    componentDidUpdate(prevProps: Props) {
        if (prevProps.constraints !== this.props.constraints) {
            this.applyConstraints();
        }
    }

    openDialog = () => {
        this.setState({isEdit: true});
    };

    closeDialog = () => {
        this.setState({isEdit: false});
    };

    applyConstraints = () => {
        const {
            minWidth,
            maxWidth,
            idealWidth,
            minHeight,
            maxHeight,
            idealHeight,
            minFrameRate,
            maxFrameRate,
            idealFrameRate
        } = this.props.constraints;

        const videoProps: any = {
            width: { min: minWidth, max: maxWidth, ideal: idealWidth },
            height: { min: minHeight, max: maxHeight, ideal: idealHeight },
            frameRate: { min: minFrameRate, max: maxFrameRate, ideal: idealFrameRate }
        };

        mediaUtils.fetchLocalMedia(
            videoProps,
            (stream: MediaStream) => {
                const video = this.videoRef.current;
                this.setState({ mediaStream: stream });

                if (video) {
                    video.srcObject = stream;
                }

                const videoTracks = stream.getVideoTracks();
                const tracksLen = videoTracks.length;
                const {height, width, frameRate} = videoTracks[0].getSettings();

                console.log(`video tracks count: ${tracksLen}, height: ${height}, width: ${width}, frameRate: ${frameRate}`);
            },
            (error: any) => {
                console.log(`Error: ${error}`)
            });
    };

    stopStream = () => {
        const { mediaStream } = this.state;
        if (mediaStream) {
            mediaStream.getTracks().forEach(track => track.stop());
        }
    };

    render() {
        const {isEdit} = this.state;

        const headerActions = (
            <Button.Group>
                <Button type="primary" onClick={this.openDialog}>Settings</Button>
                <Button onClick={this.stopStream}>Stop</Button>
            </Button.Group>
        );

        return (
            <>
                <PageHeader actions={headerActions}>
                    Get User Media Constraints
                </PageHeader>

                <UserMediaSettings isOpen={isEdit} onClose={this.closeDialog} />

                <Video ref={this.videoRef} autoPlay></Video>
            </>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        constraints: getMediaConstraints(state)
    };
};

export default connect(mapStateToProps)(UserMediaConstraints);