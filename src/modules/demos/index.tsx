import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux'

import UserMediaConstraints from './components/user-media-constraints';
import UserMediaBandwith from './components/user-media-bandwith';

import * as routes from '../../routes';
import store from './store';

const Demos = (props: { match: any }) => (
    <Provider store={store}>
        <Switch>
            <Route
                exact={true}
                path={`${props.match.url}/`}
                render={() =>
                    <Redirect to={`${props.match.url}/${routes.USER_MEDIA_CONSTRAINTS}`} />
                }
            />
            <Route path={`${props.match.url}/${routes.USER_MEDIA_CONSTRAINTS}`} component={UserMediaConstraints} />
            <Route path={`${props.match.url}/${routes.USER_MEDIA_BANDWIDTH}`} component={UserMediaBandwith} />
        </Switch>
    </Provider>
);

export default Demos;
