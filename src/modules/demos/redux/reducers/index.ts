import { combineReducers } from 'redux';
import mediaConstraints from './media-constraints';
import sdpBandwith from './sdp-bandwith';

const rootReducer = combineReducers({
    mediaConstraints,
    sdpBandwith
});

export default rootReducer;
