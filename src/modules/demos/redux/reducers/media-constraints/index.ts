import { handleActions } from 'redux-actions';
import * as actionTypes from '../../constants/action-types';

const INITIAL_STATE = {
    minWidth: 0, 
    maxWidth: 0, 
    idealWidth: 0,
    minHeight: 0, 
    maxHeight: 0, 
    idealHeight: 0,
    minFrameRate: 0, 
    maxFrameRate: 0, 
    idealFrameRate: 0
};

const mediaConstraints = handleActions({
    [actionTypes.EDIT_USER_MEDIA_CONSTRAINTS]: (state, action) => ({
        ...state,
        ...action.payload
    })
}, INITIAL_STATE);

export default mediaConstraints;
