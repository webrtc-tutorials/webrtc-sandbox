import { handleActions } from 'redux-actions';
import * as actionTypes from '../../constants/action-types';

const INITIAL_STATE = {
    bandwith: 0
};

const sdpBandwith = handleActions({
    [actionTypes.EDIT_SDP_BANDWITH]: (state, action) => ({
        ...state,
        ...action.payload
    })
}, INITIAL_STATE);

export default sdpBandwith;