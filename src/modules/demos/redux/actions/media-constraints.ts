import { createAction } from 'redux-actions';
import * as actionTypes from '../constants/action-types';

export const editUserMediaConstraints = createAction(actionTypes.EDIT_USER_MEDIA_CONSTRAINTS);