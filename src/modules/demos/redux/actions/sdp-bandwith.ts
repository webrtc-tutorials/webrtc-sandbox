import { createAction } from 'redux-actions';
import * as actionTypes from '../constants/action-types';

export const editSdpBandwith = createAction(actionTypes.EDIT_SDP_BANDWITH);