interface PropValue {
    min: number;
    max: number;
}

interface VideoProps {
    width: PropValue;
    height: PropValue;
    frameRate: PropValue;
}

// - 720p: 1280x720
// - 480p: 854x480
// - 360p: 640x360
// - 240p: 426x240

const frameRate: PropValue = {
    min: 10,
    max: 30
};

const lowVideoProps: VideoProps = {
    width: {
        min: 426,
        max: 640
    },
    height: {
        min: 240,
        max: 360
    },
    frameRate
};

const mediumVideoProps: VideoProps = {
    width: {
        min: 640,
        max: 854
    },
    height: {
        min: 360,
        max: 480
    },
    frameRate
};

const highVideoProps: VideoProps = {
    width: {
        min: 854,
        max: 1280
    },
    height: {
        min: 480,
        max: 720
    },
    frameRate
};

function convertVideoProps(props: VideoProps): MediaStreamConstraints {
    return {
        audio: true,
        video: {
            facingMode: "user",
            width: props.width,
            height: props.height,
            frameRate: props.frameRate
        }
    };
}

function fetchLocalMedia(props: VideoProps, onSuccess: (stream: MediaStream) => void, onError: (error: any) => void) {
    const constraints: MediaStreamConstraints = convertVideoProps(props);
    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess).catch(onError);
}

export {
    lowVideoProps,
    mediumVideoProps,
    highVideoProps,
    fetchLocalMedia
};