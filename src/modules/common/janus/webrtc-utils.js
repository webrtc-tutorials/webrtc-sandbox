const videoSettings = {
    height: {
        min: 0,
        max: 0,
    },
    width: {
        min: 0,
        max: 0,
    },
    frameRate: {
        max: 30,
    },
    facingMode: 'user',
};

function applyResolution(hMin, hMax, wMin, wMax) {
    return {
        ...videoSettings,
        height: {
            min: hMin,
            max: hMax
        },
        width: {
            min: wMin,
            max: wMax
        }
    };
}

export function getVideoConfig(media) {
    if (media.video === 'lowres') {
        // Small resolution, 4:3
        return applyResolution(240, 240, 320, 320);
    } else if (media.video === 'lowres-16:9') {
        // Small resolution, 16:9
        return applyResolution(180, 180, 320, 320);
    } else if (media.video === 'hires' || media.video === 'hires-16:9' || media.video === 'hdres') {
        // High(HD) resolution is only 16:9
        return applyResolution(720, 720, 1280, 1280);
    } else if (media.video === 'fhdres') {
        // Full HD resolution is only 16:9
        return applyResolution(1080, 1080, 1920, 1920);
    } else if (media.video === '4kres') {
        // 4K resolution is only 16:9
        return applyResolution(2160, 2160, 3840, 3840);
    } else if (media.video === 'stdres') {
        // Normal resolution, 4:3
        return applyResolution(480, 480, 640, 640);
    } else if (media.video === 'stdres-16:9') {
        // Normal resolution, 16:9
        return applyResolution(360, 360, 640, 640);
    } else {
        return applyResolution(480, 480, 640, 640);
    }
}
