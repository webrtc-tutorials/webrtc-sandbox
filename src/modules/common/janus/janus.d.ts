/**
 * @see https://github.com/meetecho/janus-gateway/blob/master/npm/janus.d.ts
 */
declare namespace JanusJS {
    interface Dependencies {
        adapter: any;
        newWebSocket(server: string, protocol: string): WebSocket;
        isArray(array: any): array is any[];
        checkJanusExtension(): boolean;
        httpAPICall(url: string, options: any): void;
    }

    enum DebugLevel {
        Trace = 'trace',
        Debug = 'debug',
        Log = 'log',
        Warning = 'warn',
        Error = 'error',
    }

    interface JSEP {}

    interface InitOptions {
        debug?: boolean | 'all' | DebugLevel[];
        callback?: Function;
        dependencies?: Dependencies;
    }

    interface ConstuctorOptions extends RTCConfiguration {
        server: string | string[];
        ipv6?: boolean;
        withCredentials?: boolean;
        max_poll_events?: number;
        destroyOnUnload?: boolean;
        token?: string;
        apisecret?: string;
        success?: Function;
        error?(error: any): void;
        destroyed?: Function;
    }

    enum MessageType {
        Recording = 'recording',
        Stopped = 'stopped',
        SlowLink = 'slow_link',
        Preparing = 'preparing',
        Refreshing = 'refreshing',
    }

    interface Message {
        result?: {
            status: MessageType;
            id?: string;
            uplink?: number;
        };
    }

    type VideoCodec = 'vp8' | 'vp9' | 'h264';

    type AudioCodec = 'opus' | 'g722' | 'pcmu' | 'pcma' | 'isac32' | 'isac16';

    enum VideoRoomEvent {
        JOINED = 'joined',
        DESTROYED = 'destroyed',
        EVENT = 'event',
        CREATED = 'created',
        EDITED = 'edited',
        SUCCESS = 'success',
        PARTICIPANTS = 'participants',
        ATTACHED = 'attached',
        RTP_FORWARD = 'rtp_forward',
        STOP_RTP_FORWARD = 'stop_rtp_forward',
        FORWARDERS = 'forwarders',
    }

    interface VideoPluginMessage extends Message {
        id?: number;
        display?: string;
        room?: string;
        temporal?: number;
        substream?: number;
        error?: string;
        videoroom?: VideoRoomEvent;
        error_code?: number;
        leaving?: 'ok' | number;
        unpublished?: 'ok' | number;
        audio_codec?: AudioCodec;
        video_codec?: VideoCodec;
        private_id?: number;
        publishers?: VideoPluginMessage[];
    }

    interface JoinMessage {
        request : 'join';
        ptype : 'subscriber';

        /** <unique ID of the room to subscribe in> */
        room: number;

        /** <unique ID of the publisher to subscribe to; mandatory> */
        feed: number;

        /** optional password needed for joining the room */
        pin?: string;

        /**
         * <unique ID of the publisher that originated this request;
         * optional, unless mandated by the room configuration>
         * */
        private_id?: number;

        /**
         * <depending on whether or not the PeerConnection should be automatically closed when the publisher leaves; true by default>
         * */
        close_pc?: boolean;
        /** depending on whether or not audio should be relayed; true by default>, */
        audio?: boolean;
        /** depending on whether or not video should be relayed; true by default>, */
        video?: boolean;
        /** depending on whether or not data should be relayed; true by default>, */
        data?: boolean;
        /**  whether or not audio should be negotiated; true by default if the publisher has audio>, */
        offer_audio?: boolean;
        /**  whether or not video should be negotiated; true by default if the publisher has video>, */
        offer_video?: boolean;
        /**  whether or not datachannels should be negotiated; true by default if the publisher has datachannels> */
        offer_data?: boolean;
    }

    interface PluginOptions {
        plugin: string;
        opaqueId?: string;

        /**
         * The handle was successfully created and is ready to be used
         */
        success?(handle: PluginHandle): void;

        /**
         * The handle was NOT successfully created
         */
        error?(error: any): void;

        /**
         * This callback is triggered just before getUserMedia is called (parameter=true)
         * and after it is completed (parameter=false);
         * this means it can be used to modify the UI accordingly, e.g.,
         * to prompt the user about the need to accept the device access consent requests
         */
        consentDialog?(on: boolean): void;

        /**
         * This callback is triggered with a true value when the PeerConnection
         * associated to a handle becomes active (so ICE, DTLS and everything else succeeded)
         * from the Janus perspective, while false is triggered
         * when the PeerConnection goes down instead;
         * useful to figure out when WebRTC is actually up and running between you and Janus
         * (e.g., to notify a user they're actually now active in a conference);
         * notice that in case of false a reason string may be present
         * as an optional parameter;
         */
        webrtcState?(isConnected: boolean): void;

        /**
         * This callback is triggered when the ICE state for the PeerConnection
         * associated to the handle changes: the argument of the callback
         * is the new state as a string (e.g., "connected" or "failed");
         */
        iceState?(state: 'connected' | 'failed'): void;

        /**
         * This callback is triggered when Janus starts or stops receiving your media:
         * for instance, a mediaState with type=audio and on=true means
         * Janus started receiving your audio stream
         * (or started getting them again after a pause of more than a second);
         * a mediaState with type=video and on=false means
         * Janus hasn't received any video from you in the last second,
         * after a start was detected before;
         * useful to figure out when Janus actually started handling your media,
         * or to detect problems on the media path
         * (e.g., media never started, or stopped at some time);
         */
        mediaState?(state: { type: 'audio' | 'video'; on: boolean }): void;

        /**
         * This callback is triggered when Janus reports trouble either sending
         * or receiving media on the specified PeerConnection,
         * typically as a consequence of too many NACKs received from/sent to the user
         * in the last second: for instance, a slowLink with uplink=true means
         * you notified several missing packets from Janus,
         * while uplink=false means Janus is not receiving all your packets;
         * useful to figure out when there are problems on the media path
         * (e.g., excessive loss), in order to possibly react accordingly
         * (e.g., decrease the bitrate if most of our packets are getting lost);
         */
        slowLink?(state: { uplink: boolean }): void;

        /**
         * We got a message/event (msg) from the plugin.
         * If jsep is not null, this involves a WebRTC negotiation
         */
        onmessage?(message: VideoPluginMessage, jsep?: JSEP): void;

        /**
         * A local MediaStream is available and ready to be displayed
         */
        onlocalstream?(stream: MediaStream): void;

        /**
         * A remote MediaStream is available and ready to be displayed
         */
        onremotestream?(stream: MediaStream): void;

        /**
         * A Data Channel is available and ready to be used
         */
        ondataopen?: Function;

        /**
         * Data has been received through the Data Channel
         */
        ondata?: Function;

        /**
         * The WebRTC PeerConnection with the plugin was closed
         * The plugin handle is still valid so we can create a new one
         */
        oncleanup?: Function;

        /**
         * The plugin handle has been detached by the plugin itself,
         * and so should not be used anymore
         */
        detached?: Function;
    }

    interface OfferParams {
        simulcast?: boolean;
        simulcast2?: boolean;
        /**
         * by default audio and video are enabled in both directions,
         * while the Data Channels are disabled;
         * this option is an object that can take any of the following properties
         */
        media?: {
            audioSend?: boolean;
            audioRecv?: boolean;
            videoSend?: boolean;
            videoRecv?: boolean;
            audio?: boolean | { deviceId: string };
            video?:
                | boolean
                | { deviceId: string }
                | 'lowres'
                | 'lowres-16:9'
                | 'stdres'
                | 'stdres-16:9'
                | 'hires'
                | 'hires-16:9'
                | 'fhdres'
                | '4kres';
            data?: boolean;
            failIfNoAudio?: boolean;
            failIfNoVideo?: boolean;
            screenshareFrameRate?: number;
        };
        trickle?: boolean;
        stream?: MediaStream;
        success: Function;
        error(error: any): void;
    }

    interface PluginMessage {
        message: JoinMessage | {
            request: string;
            [otherProps: string]: any;
        };
        jsep?: JSEP;
        success?: (data: object) => void;
    }

    interface PluginHandle {
        webrtcStuff: {
            pc: {
                iceConnectionState?: 'completed' | 'connected',
            };
        };
        isAudioMuted(): boolean;
        isVideoMuted(): boolean;
        muteAudio(): void;
        unmuteAudio(): void;
        muteVideo(): void;
        unmuteVideo(): void;
        getId(): string;
        getPlugin(): string;
        send(message: PluginMessage): void;
        createOffer(params: OfferParams): void;
        createAnswer(params: any): void;
        handleRemoteJsep(params: { jsep: JSEP }): void;
        dtmf(params: any): void;
        data(params: any): void;
        getBitrate(): number;
        hangup(sendRequest?: boolean): void;
        detach(params?: any): void;
    }
}

class JanusJS {
    static safariVp8: boolean;
    static sessions: Record<string, JanusJS>;
    static useDefaultDependencies(deps: Partial<Dependencies>): Dependencies;
    static useOldDependencies(deps: Partial<Dependencies>): Dependencies;
    static init(options: InitOptions): void;
    static isWebrtcSupported(): boolean;
    static debug(...args: any[]): void;
    static log(...args: any[]): void;
    static warn(...args: any[]): void;
    static error(...args: any[]): void;
    static randomString(length: number): string;
    static attachMediaStream(element: HTMLElement, stream: MediaStream): void;
    static webRTCAdapter: {
        browserDetails: {
            browser: 'chrome' | 'safari' | 'firefox' | 'edge'
        }
    };

    constructor(options: ConstuctorOptions);

    getServer(): string;
    isConnected(): boolean;
    getSessionId(): string;
    attach(options: PluginOptions): void;
    destroy(): void;
}

export { JanusJS };
