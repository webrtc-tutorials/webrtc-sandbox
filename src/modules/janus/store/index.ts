import { makeStore } from '../../../util/store';
import rootReducer from '../redux/reducers';

const store = makeStore(rootReducer);

export default store;