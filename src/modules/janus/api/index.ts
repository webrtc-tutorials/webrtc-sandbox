import axios from "axios";
import uuid from 'uuid/v4';

const API_BASE_URL = "https://dev.server.com/api";

const client = (url) => axios.create({
    baseURL: API_BASE_URL + url,
    headers: {
        "Content-Type": "application/json",
    },
});

const janusClient = client("/rtc-http");

export function createSession() {
    const transaction = uuid();

    const message = {
        janus: "create",
        transaction
    };

    return janusClient.post("/", message).then(res => res.data);
}

export function attachVideoPlugin(sessionId: string) {
    const transaction = uuid();

    const message = {
        janus: "attach",
        plugin: "",
        transaction
    };

    return janusClient.post(`/${sessionId}`, message).then(res => res.data);
}

export function listVideoRooms(sessionId: string, pluginId: string) {
    const transaction = uuid();

    const message = {
        janus: "message",
        transaction,
        body: {
            request: "list"
        }
    };

    return janusClient.post(`/${sessionId}/${pluginId}`, message).then(res => res.data);
}
