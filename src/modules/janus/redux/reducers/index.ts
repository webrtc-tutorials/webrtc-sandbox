import { combineReducers } from 'redux';
import mediaConstraints from './media-constraints';

const rootReducer = combineReducers({
    mediaConstraints
});

export default rootReducer;
