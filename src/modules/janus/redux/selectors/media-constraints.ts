import { RootStore } from '../../typings';

export const getVideoConstraints = (store: RootStore) => store.mediaConstraints.video;