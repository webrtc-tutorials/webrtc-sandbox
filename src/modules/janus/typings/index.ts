export interface VideoConstraints {
    width?: ConstrainULongRange,
    height?: ConstrainULongRange,
    frameRate?: ConstrainDoubleRange
}

export interface RootStore {
    mediaConstraints: MediaConstraintsStore;
}

export interface MediaConstraintsStore {
    video: VideoConstraints;
}

export enum MessageType {
    log = 'log',
    message = 'message',
    createOrJoin = 'create-or-join',
    created = 'created',
    joined = 'joined',
    remotePeerJoining = 'remote-peer-joining',
    broadcastJoined = 'broadcast-joined',
    full = 'full',
    response = 'response',
    bye = 'bye',
    ack = 'ack',
    gotUserMedia = 'got-user-media'
}

export enum MessageSubType {
    offer = 'offer',
    answer = 'answer',
    candidate = 'candidate'
}

export interface Message {
    channel: string;
    message: string;
}

export interface VideoRoom {
    audiocodec: string;
    bitrate: number;
    description: string;
    fir_freq: number;
    max_publishers: number;
    notify_joining: boolean;
    num_participants: number;
    pin_required: boolean;
    rec_dir: string;
    record: boolean;
    require_pvtid: boolean;
    room: number;
    videocodec: string;
}

// constants for video modes
const frameRate: ConstrainDoubleRange = {
    min: 10,
    max: 30
};

export const lowVideoProps: VideoConstraints = {
    width: {
        min: 426,
        max: 640
    },
    height: {
        min: 240,
        max: 360
    },
    frameRate
};

export const mediumVideoProps: VideoConstraints = {
    width: {
        min: 640,
        max: 854
    },
    height: {
        min: 360,
        max: 480
    },
    frameRate
};

export const highVideoProps: VideoConstraints = {
    width: {
        min: 854,
        max: 1280
    },
    height: {
        min: 480,
        max: 720
    },
    frameRate
};
