import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import io from 'socket.io-client';

import VideoRoomPage from './components/video-room';
import RoomsListPage from './components/rooms-list/rooms-list';

import SocketContext from './context/socket-context';

import * as routes from '../../routes';
import store from './store';
import {Provider} from "react-redux";

const socket = io.connect('https://server.com');

const Janus = (props: { match: any }) => {
    return (
        <SocketContext.Provider value={socket}>
            <Provider store={store}>
                <Switch>
                    <Route
                        exact={true}
                        path={`${props.match.url}/`}
                        render={() =>
                            <Redirect to={`${props.match.url}/${routes.VIDEO_ROOM}`} />
                        }
                    />
                    <Route path={`${props.match.url}/${routes.VIDEO_ROOM}`} component={VideoRoomPage} />
                    <Route path={`${props.match.url}/${routes.ROOMS_LIST}`} component={RoomsListPage} />
                </Switch>
            </Provider>
        </SocketContext.Provider>
    );
};

export default Janus;
