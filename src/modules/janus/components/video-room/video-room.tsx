import React, {RefObject} from 'react';
import {Divider} from 'antd';
import {connect} from 'react-redux';
import adapterModule from 'webrtc-adapter/out/adapter_no_edge';

import PageHeader from '../../../../components/common/PageHeader';
import {LocalVideo, RemoteVideo} from './video-room.styled';
import JoinChannel from './join-channel';

import {VideoConstraints, RootStore} from '../../typings';
import {getVideoConstraints} from '../../redux/selectors/media-constraints';

import {JanusJS} from '../../../common/janus/janus';

const ICE_SERVERS: RTCIceServer[] = [
    {urls: 'stun:stun01.sipphone.com'},
    {urls: 'stun:stun.ekiga.net'},
    {urls: 'stun:stun.fwdnet.net'},
    {urls: 'stun:stun.ideasip.com'},
    {urls: 'stun:stun.iptel.org'},
    {urls: 'stun:stun.rixtelecom.se'},
    {urls: 'stun:stun.schlund.de'},
    {urls: 'stun:stun.l.google.com:19302'},
    {urls: 'stun:stun1.l.google.com:19302'},
    {urls: 'stun:stun2.l.google.com:19302'},
    {urls: 'stun:stun3.l.google.com:19302'},
    {urls: 'stun:stun4.l.google.com:19302'},
    {urls: 'stun:stunserver.org'},
    {urls: 'stun:stun.softjoys.com'},
    {urls: 'stun:stun.voiparound.com'},
    {urls: 'stun:stun.voipbuster.com'},
    {urls: 'stun:stun.voipstunt.com'},
    {urls: 'stun:stun.voxgratia.org'},
    {urls: 'stun:stun.xten.com'},
    {
        urls: 'turn:numb.viagenie.ca',
        credential: 'muazkh',
        username: 'webrtc@live.com',
    },
    {
        urls: 'turn:192.158.29.39:3478?transport=udp',
        credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        username: '28224511:1379330808',
    },
    {
        urls: 'turn:192.158.29.39:3478?transport=tcp',
        credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        username: '28224511:1379330808',
    },
];

interface Props {
    socket: SocketIOClient.Socket;
    videoConstraints: VideoConstraints;
}

interface State {
    constraints?: VideoConstraints;
    sessionId?: string;
    pluginId?: string;
    isReady: boolean;
    isJoined: boolean;
}

class VideoRoom extends React.Component<Props, State> {

    props: Props;

    readonly state: State = {
        isReady: false,
        isJoined: false
    };

    private localVideoRef = React.createRef<HTMLVideoElement>();
    private remoteVideoRef = React.createRef<HTMLVideoElement>();

    private janusSession: JanusJS;
    private plugin: JanusJS.PluginHandle;

    private myStream?: MediaStream;
    private myId?: number;
    private myPrivateId?: number;
    private myRoom?: number = 1234;

    private feeds: Array<any> = [];

    private opaqueId: string = `videoroomtest-${JanusJS.randomString(12)}`;

    private attachMediaStream = (elementRef: RefObject<HTMLVideoElement>, stream: MediaStream, muted: boolean = false) => {
        const videoTag = elementRef.current;

        if (videoTag) {
            videoTag.srcObject = stream;
            videoTag.muted = muted;
        }
    };

    // init janus client
    private initRtc() {
        JanusJS.init({
            debug: true,
            dependencies: JanusJS.useDefaultDependencies({
                adapter: adapterModule.default,
            }),
            callback: this.startSession
        });
    }

    // create a session
    private startSession = () => {
        // Make sure the browser supports WebRTC
        if (!JanusJS.isWebrtcSupported()) {
            alert('No WebRTC support... ');
            return;
        }

        this.janusSession = new JanusJS({
            iceServers: ICE_SERVERS,
            error: (error: string) => {
                JanusJS.error(error);
            },
            server: [
                'wss://dev.server.com/api/rtc-ws', // apiServices.rtcWebSocketService.defaults.baseURL,
                'https://dev.server.com/api/rtc-http', // apiServices.rtcLongPoolingService.defaults.baseURL,
            ],
            // will be invoked even if request for destroy was failed
            destroyed: () => {
                JanusJS.log('>> session destroyed');
            },
            success: this.attachPlugin,
        });
    };

    // attach video room plugin
    private attachPlugin = () => {
        const self = this;
        const sessionId = this.janusSession.getSessionId();

        this.setState({sessionId});

        JanusJS.log('>> sessionID: ' + sessionId);

        const jSession = JanusJS.sessions[sessionId];

        jSession.attach({
            plugin: "janus.plugin.videoroom",
            success: (pluginHandle) => {
                this.plugin = pluginHandle;

                JanusJS.log(`Plugin attached! (${this.plugin.getPlugin()}, id=${this.plugin.getId()})`);
                JanusJS.log("  -- This is a publisher/manager");

                JanusJS.debug(`SessionId: ${sessionId}, pluginId: ${this.plugin.getId()}`);

                this.setState({
                    isReady: true,
                    pluginId: this.plugin.getId()
                })
            },
            mediaState: (medium, on) => {
                JanusJS.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
            },
            webrtcState: (on) => {
                JanusJS.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
            },
            onmessage: this.processMessage,
            onlocalstream: function (stream) {
                JanusJS.log(" ::: Got a local stream :::");

                self.myStream = stream;

                JanusJS.log(stream);

                self.attachMediaStream(self.localVideoRef, stream);
            },
            oncleanup: function () {
                JanusJS.log(" ::: Got a cleanup notification: we are unpublished now :::");
                self.myStream = undefined;
            }
        })
    };

    // process janus message
    private processMessage = (message: JanusJS.VideoPluginMessage, jsep: JanusJS.JSEP) => {
        JanusJS.log(" ::: Got a message (publisher) :::");
        JanusJS.log(message);

        const event = message.videoroom;

        JanusJS.log("Event: " + event);

        if (event !== undefined && event !== null) {
            this.processEvent(message, event);
        }

        if (jsep !== undefined && jsep !== null) {
            this.processJSEP(message, jsep);
        }
    };

    // process janus message: event part
    private processEvent = (message: JanusJS.VideoPluginMessage, event: string) => {
        switch (event) {
            case 'joined':
                // Publisher/manager created, negotiate WebRTC and attach to existing feeds, if any
                this.myId = message.id;
                this.myPrivateId = message.private_id;

                JanusJS.log(`Successfully joined Room: ${message.room}, with ID: ${this.myId}`);
                JanusJS.log(message);

                this.publishOwnFeed(true);

                // Any new feed to attach to?
                if (message.publishers) {
                    const list = message.publishers;

                    JanusJS.log("Got a list of available publishers/feeds:");
                    JanusJS.log(list);

                    for (let field in list) {
                        const {id, display, audio_codec, video_codec} = list[field];
                        JanusJS.log(`  >> [${id}] ${display} (audio: ${audio_codec}, video: ${video_codec})`);
                        this.newRemoteFeed(id, display, audio_codec, video_codec);
                    }
                }

                break;
            case 'destroyed':
                // The room has been destroyed
                JanusJS.log("The room has been destroyed!");
                alert("The room has been destroyed");
                break;
            case 'event':
                this.processSubEvent(message);
                break;
        }
    };

    // process janus message: if event = 'event'
    private processSubEvent = (message: JanusJS.VideoPluginMessage) => {
        if (message.publishers) {
            const list = message.publishers;

            JanusJS.log("Got a list of available publishers/feeds:");
            JanusJS.log(list);

            for (let field in list) {
                const {id, display, audio_codec, video_codec} = list[field];
                JanusJS.log(`  >> [${id}] ${display} (audio: ${audio_codec}, video: ${video_codec})`);

                this.newRemoteFeed(id, display, audio_codec, video_codec);
            }
        } else if (message.leaving) {
            // One of the publishers has gone away?
            const leaving = message.leaving;
            JanusJS.log("Publisher left: " + leaving);

            this.processLeave(leaving);
        } else if (message.unpublished) {
            // One of the publishers has unpublished?
            const unpublished = message.unpublished;

            JanusJS.log("Publisher left: " + unpublished);

            if (unpublished === 'ok') {
                // That's us
                this.plugin.hangup();
                return;
            }

            this.processLeave(unpublished);
        } else if (message.error) {
            if (message.error_code === 426) {
                const errorMessage = `<p>Apparently room <code>${this.myRoom}</code> (the one this demo uses as a test room)
                    does not exist...</p><p>Do you have an updated <code>janus.plugin.videoroom.cfg</code>
                    configuration file? If not, make sure you copy the details of room <code>${this.myRoom}</code>
                    from that sample in your current configuration file, then restart Janus and try again.`;

                // This is a "no such room" error: give a more meaningful description
                alert(errorMessage);
            } else {
                alert(message.error);
            }
        }
    };

    private processLeave = (publisher: any) => {
        let remoteFeed;

        for (let i = 1; i < 6; i++) {
            if (this.feeds[i] && this.feeds[i].rfid === publisher) {
                remoteFeed = this.feeds[i];
                break;
            }
        }

        if (remoteFeed) {
            JanusJS.debug("Feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") has left the room, detaching");

            this.feeds[remoteFeed.rfindex] = null;
            remoteFeed.detach();
        }
    };

    // process janus message: JSEP part
    private processJSEP = (message: JanusJS.VideoPluginMessage, jsep: JanusJS.JSEP) => {
        JanusJS.log("Handling SDP as well...");
        JanusJS.log(jsep);

        this.plugin.handleRemoteJsep({jsep});

        // Check if any of the media we wanted to publish has
        // been rejected (e.g., wrong or unsupported codec)
        const {audio_codec, video_codec} = message;

        if (this.myStream && this.myStream.getAudioTracks() && this.myStream.getAudioTracks().length > 0 && !audio_codec) {
            // Audio has been rejected
            alert("Our audio stream has been rejected, viewers won't hear us");
        }

        if (this.myStream && this.myStream.getVideoTracks() && this.myStream.getVideoTracks().length > 0 && !video_codec) {
            // Video has been rejected
            alert("Our video stream has been rejected, viewers won't see us");
        }
    };

    // publish local video
    private publishOwnFeed = (useAudio: boolean) => {
        const self = this;

        self.plugin.createOffer(
            {
                // Add data:true here if you want to publish datachannels as well
                media: {audioRecv: false, videoRecv: false, audioSend: useAudio, videoSend: true},	// Publishers are sendonly
                // If you want to test simulcasting (Chrome and Firefox only), then
                // pass a ?simulcast=true when opening this demo page: it will turn
                // the following 'simulcast' property to pass to janus.js to true
                simulcast: true,
                simulcast2: true,
                success: (jsep) => {
                    JanusJS.log("Got publisher SDP!");
                    JanusJS.log(jsep);

                    const message = {"request": "configure", "audio": useAudio, "video": true};

                    // You can force a specific codec to use when publishing by using the
                    // audiocodec and videocodec properties, for instance:
                    // 		publish["audiocodec"] = "opus"
                    // to force Opus as the audio codec to use, or:
                    // 		publish["videocodec"] = "vp9"
                    // to force VP9 as the videocodec to use. In both case, though, forcing
                    // a codec will only work if: (1) the codec is actually in the SDP (and
                    // so the browser supports it), and (2) the codec is in the list of
                    // allowed codecs in a room. With respect to the point (2) above,
                    // refer to the text in janus.plugin.videoroom.cfg for more details

                    self.plugin.send({message, jsep});
                },
                error: (error) => {
                    JanusJS.log("WebRTC error:", error);

                    if (useAudio) {
                        self.publishOwnFeed(false);
                    } else {
                        alert("WebRTC error... " + JSON.stringify(error));
                    }
                }
            });
    };

    // add remote video
    private newRemoteFeed = (id, display, audio_codec, video_codec) => {
        // A new feed has been published, create a new plugin handle and attach to it as a subscriber
        const self = this;
        let remoteFeed;

        this.janusSession.attach(
            {
                plugin: "janus.plugin.videoroom",
                opaqueId: self.opaqueId,
                success: (pluginHandle) => {
                    remoteFeed = pluginHandle;
                    remoteFeed.simulcastStarted = false;

                    JanusJS.log("Plugin attached! (" + remoteFeed.getPlugin() + ", id=" + remoteFeed.getId() + ")");
                    JanusJS.log("  -- This is a subscriber");

                    // We wait for the plugin to send us an offer
                    let offer_video: boolean = true;

                    // In case you don't want to receive audio, video or data, even if the
                    // publisher is sending them, set the 'offer_audio', 'offer_video' or
                    // 'offer_data' properties to false (they're true by default), e.g.:
                    // 		subscribe["offer_video"] = false;
                    // For example, if the publisher is VP8 and this is Safari, let's avoid video
                    if (JanusJS.webRTCAdapter.browserDetails.browser === "safari"
                            && (video_codec === "vp9" || (video_codec === "vp8" && !JanusJS.safariVp8))) {
                        if (video_codec) {
                            video_codec = video_codec.toUpperCase();
                        }

                        JanusJS.log("Publisher is using " + video_codec + ", but Safari doesn't support it: disabling video");
                        offer_video = false;
                    }

                    const message = {
                        request: "join",
                        room: self.myRoom,
                        ptype: "subscriber",
                        feed: id,
                        private_id: self.myPrivateId,
                        offer_video
                    };

                    remoteFeed.videoCodec = video_codec;
                    remoteFeed.send({ message });
                },
                error: (error) => {
                    JanusJS.log("  -- Error attaching plugin...", error);
                    alert("Error attaching plugin... " + error);
                },
                onmessage: (message: JanusJS.VideoPluginMessage, jsep: JanusJS.JSEP) => {
                    JanusJS.log(" ::: Got a message (subscriber) :::");
                    JanusJS.log(message);

                    const event = message.videoroom;

                    JanusJS.log("Event: " + event);

                    if (message.error) {
                        alert(message.error);
                    } else if (event) {
                        if (event === "attached") {
                            // Subscriber created and attached
                            for (let i = 1; i < 6; i++) {
                                if (self.feeds[i]) {
                                    self.feeds[i] = remoteFeed;
                                    remoteFeed.rfindex = i;
                                    break;
                                }
                            }

                            const {id, display, room} = message;

                            remoteFeed.rfid = id;
                            remoteFeed.rfdisplay = display;

                            JanusJS.log("Successfully attached to feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") in room " + room);
                        } else if (event === "event") {
                            // Check if we got an event on a simulcast-related event from this publisher
                        } else {
                            // What has just happened?
                        }
                    }

                    if (jsep !== undefined && jsep !== null) {
                        JanusJS.log("Handling SDP as well...");
                        JanusJS.log(jsep);

                        // Answer and attach
                        remoteFeed.createAnswer(
                            {
                                jsep: jsep,
                                // Add data:true here if you want to subscribe to datachannels as well
                                // (obviously only works if the publisher offered them in the first place)
                                media: {audioSend: false, videoSend: false},	// We want recvonly audio/video
                                success: (jsep) => {
                                    JanusJS.debug("Got SDP!");
                                    JanusJS.debug(jsep);

                                    const message = {request: "start", room: self.myRoom};

                                    remoteFeed.send({message, jsep});
                                },
                                error: (error) => {
                                    JanusJS.error("WebRTC error:", error);
                                    alert("WebRTC error... " + JSON.stringify(error));
                                }
                            });
                    }
                },
                webrtcState: (on) => {
                    JanusJS.log("Janus says this WebRTC PeerConnection (feed #" + remoteFeed.rfindex + ") is " + (on ? "up" : "down") + " now");
                },
                onlocalstream: (stream) => {
                    // The subscriber stream is recvonly, we don't expect anything here
                },
                onremotestream: (stream) => {
                    JanusJS.debug("Remote feed #" + remoteFeed.rfindex);
                    self.attachMediaStream(self.remoteVideoRef, stream);
                },
                oncleanup: () => {
                    JanusJS.log(" ::: Got a cleanup notification (remote feed " + id + ") :::");
                }
            });
    };

    // ----------------------------------------------------------------------------------- //
    //    UI LOGIC
    // ----------------------------------------------------------------------------------- //

    static getDerivedStateFromProps(nextProps: Props, prevState: State) {
        return {
            ...prevState,
            constraints: nextProps.videoConstraints
        };
    }

    componentDidMount() {
        this.initRtc();
    }

    componentWillUnmount() {
        if (this.janusSession) {
            this.janusSession.destroy();
        }
    }

    createOrJoin = (name: string) => {
        JanusJS.log(`>> Joining room`);

        const message = {
            request: "join",
            room: this.myRoom,
            ptype: "publisher",
            display: name
        };

        this.plugin.send({message});
    };

    leave = () => {
        //
    };

    render() {
        const {isJoined} = this.state;

        return (
            <>
                <PageHeader>Video Room</PageHeader>

                <JoinChannel isJoined={isJoined} onJoin={this.createOrJoin} onLeave={this.leave}/>

                <Divider dashed/>

                <div>
                    <LocalVideo ref={this.localVideoRef} autoPlay></LocalVideo>
                </div>
                <div>
                    <RemoteVideo ref={this.remoteVideoRef} autoPlay></RemoteVideo>
                </div>
            </>
        );
    }
}

const mapStateToProps = (store: RootStore) => {
    return {
        videoConstraints: getVideoConstraints(store)
    };
};

export default connect(mapStateToProps)(VideoRoom);
