import React from 'react';
import VideoRoom from './video-room';
import SocketContext from '../../context/socket-context';

const VideoRoomPage = () => (
    <SocketContext.Consumer>
        {socket => <VideoRoom socket={socket} />}
    </SocketContext.Consumer>
);

export default VideoRoomPage;
