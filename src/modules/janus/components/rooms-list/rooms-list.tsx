import React from 'react';
import { Button, Divider } from 'antd';
import adapterModule from 'webrtc-adapter/out/adapter_no_edge';

import PageHeader from '../../../../components/common/PageHeader';
import VideoRoomsListTable from './rooms-list-table';

import { JanusJS } from '../../../common/janus/janus';
import { listVideoRooms } from '../../api';
import { VideoRoom } from '../../typings';

const ICE_SERVERS: RTCIceServer[] = [
    { urls: 'stun:stun01.sipphone.com' },
    { urls: 'stun:stun.ekiga.net' },
    { urls: 'stun:stun.fwdnet.net' },
    { urls: 'stun:stun.ideasip.com' },
    { urls: 'stun:stun.iptel.org' },
    { urls: 'stun:stun.rixtelecom.se' },
    { urls: 'stun:stun.schlund.de' },
    { urls: 'stun:stun.l.google.com:19302' },
    { urls: 'stun:stun1.l.google.com:19302' },
    { urls: 'stun:stun2.l.google.com:19302' },
    { urls: 'stun:stun3.l.google.com:19302' },
    { urls: 'stun:stun4.l.google.com:19302' },
    { urls: 'stun:stunserver.org' },
    { urls: 'stun:stun.softjoys.com' },
    { urls: 'stun:stun.voiparound.com' },
    { urls: 'stun:stun.voipbuster.com' },
    { urls: 'stun:stun.voipstunt.com' },
    { urls: 'stun:stun.voxgratia.org' },
    { urls: 'stun:stun.xten.com' },
    {
        urls: 'turn:numb.viagenie.ca',
        credential: 'muazkh',
        username: 'webrtc@live.com',
    },
    {
        urls: 'turn:192.158.29.39:3478?transport=udp',
        credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        username: '28224511:1379330808',
    },
    {
        urls: 'turn:192.158.29.39:3478?transport=tcp',
        credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        username: '28224511:1379330808',
    },
];

interface Props {
}

interface State {
    isReady: boolean;
    sessionId?: string;
    pluginId?: string;
    videoRooms: Array<VideoRoom>;
}

class RoomsList extends React.Component<Props, State> {

    props: Props;

    readonly state: State = {
        isReady: false,
        videoRooms: []
    };

    private janusSession: JanusJS;
    private plugin: JanusJS.PluginHandle;

    // init janus client
    private initRtc() {
        JanusJS.init({
            debug: true,
            dependencies: JanusJS.useDefaultDependencies({
                adapter: adapterModule.default,
            }),
            callback: this.startSession
        });
    }

    // create a session
    private startSession = (onSuccess) => {
        // Make sure the browser supports WebRTC
        if (!JanusJS.isWebrtcSupported()) {
            alert('No WebRTC support... ');
            return;
        }

        this.janusSession = new JanusJS({
            iceServers: ICE_SERVERS,
            error: (error: string) => {
                JanusJS.error(error);
            },
            server: [
                'wss://dev.server.com/api/rtc-ws', // apiServices.rtcWebSocketService.defaults.baseURL,
                'https://dev.server.com/api/rtc-http', // apiServices.rtcLongPoolingService.defaults.baseURL,
            ],
            // will be invoked even if request for destroy was failed
            destroyed: () => {
                JanusJS.log('>> session destroyed');
            },
            success: this.attachPlugin,
        });
    };

    // attach video room plugin
    private attachPlugin = () => {
        const sessionId = this.janusSession.getSessionId();

        this.setState({ sessionId });

        JanusJS.log('>> sessionID: ' + sessionId);

        const jSession = JanusJS.sessions[sessionId];

        jSession.attach({
            plugin: "janus.plugin.videoroom",
            success: (pluginHandle) => {
                this.plugin = pluginHandle;

                JanusJS.log(`Plugin attached! (${this.plugin.getPlugin()}, id=${this.plugin.getId()})`);
                JanusJS.log("  -- This is a publisher/manager");

                JanusJS.log(`SessionId: ${sessionId}, pluginId: ${this.plugin.getId()}`);

                this.setState({
                    isReady: true,
                    pluginId: this.plugin.getId()
                })
            }
        })
    };

    componentDidMount() {
        this.initRtc();
    }

    componentWillUnmount() {
        if (this.janusSession) {
            this.janusSession.destroy();
        }
    }

    fetchRooms = () => {
        const { sessionId, pluginId } = this.state;

        if (!sessionId || !pluginId) return;

        listVideoRooms(sessionId, pluginId).then(resp => {
            const data = resp.plugindata.data.list;

            this.setState({
                ...this.state,
                videoRooms: data
            })
        });
    };

    render() {
        const {isReady, videoRooms} = this.state;

        return (
            <>
                <PageHeader>Video Rooms</PageHeader>

                {isReady &&
                    <>
                        <Button type="primary" onClick={this.fetchRooms}>Fetch rooms</Button>
                        <Divider dashed />

                        <VideoRoomsListTable items={videoRooms} />
                    </>
                }
            </>
        );
    }
}

export default RoomsList;
