import React from 'react';
import { Table } from "antd";

import { VideoRoom } from '../../typings';

class VideoRoomsTable extends Table<VideoRoom> {
}

const VideoRoomsListTable = ({items}) => {

    return (
        <VideoRoomsTable rowKey="room" dataSource={items} pagination={false} size="middle">
            <VideoRoomsTable.Column key="room" title="Room ID" dataIndex="room" />
            <VideoRoomsTable.Column key="description" title="Room Name" dataIndex="description" />
            <VideoRoomsTable.Column key="num_participants" title="No of Participants" dataIndex="num_participants" />
            <VideoRoomsTable.Column key="videocodec" title="Video Codec" dataIndex="videocodec" />
            <VideoRoomsTable.Column key="audiocodec" title="Audio Codec" dataIndex="audiocodec" />
        </VideoRoomsTable>
    );
};

export default VideoRoomsListTable;
