import React from 'react';
import RoomsList from './rooms-list';

const RoomsListPage = () => (
    <RoomsList />
);

export default RoomsListPage;
