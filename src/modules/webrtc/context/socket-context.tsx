import React from 'react';

const SocketContext = React.createContext<any>({});

export default SocketContext;
