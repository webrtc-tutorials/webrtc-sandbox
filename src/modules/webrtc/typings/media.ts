import { VideoConstraints } from '../redux/typings';

// - 720p: 1280x720
// - 480p: 854x480
// - 360p: 640x360
// - 240p: 426x240

const frameRate: ConstrainDoubleRange = {
    min: 10,
    max: 30
};

export const lowVideoProps: VideoConstraints = {
    width: {
        min: 426,
        max: 640
    },
    height: {
        min: 240,
        max: 360
    },
    frameRate
};

export const mediumVideoProps: VideoConstraints = {
    width: {
        min: 640,
        max: 854
    },
    height: {
        min: 360,
        max: 480
    },
    frameRate
};

export const highVideoProps: VideoConstraints = {
    width: {
        min: 854,
        max: 1280
    },
    height: {
        min: 480,
        max: 720
    },
    frameRate
};
