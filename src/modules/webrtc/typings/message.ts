export enum MessageType {
    log = 'log',
    message = 'message',
    createOrJoin = 'create-or-join',
    created = 'created',
    joined = 'joined',
    remotePeerJoining = 'remote-peer-joining',
    broadcastJoined = 'broadcast-joined',
    full = 'full',
    response = 'response',
    bye = 'bye',
    ack = 'ack',
    gotUserMedia = 'got-user-media'
}

export enum MessageSubType {
    offer = 'offer',
    answer = 'answer',
    candidate = 'candidate'
}

export interface Message {
    channel: string;
    message: string;
}
