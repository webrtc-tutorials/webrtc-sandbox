import { handleActions } from 'redux-actions';
import * as actionTypes from '../../constants/action-types';
import { BandwidthStore } from '../../typings';

const INITIAL_STATE: BandwidthStore = {
    bandwidth: 0
};

const sdpBandwidth = handleActions<BandwidthStore, number>({
    [actionTypes.EDIT_SDP_BANDWIDTH]: (state, action) => ({
        bandwidth: action.payload
    })
}, INITIAL_STATE);

export default sdpBandwidth;
