import { handleActions } from 'redux-actions';
import * as actionTypes from '../../constants/action-types';
import { VideoConstraints, MediaConstraintsStore } from '../../typings';
import { highVideoProps } from '../../../typings/media';

const INITIAL_STATE: MediaConstraintsStore = {
    video: highVideoProps
};

const mediaConstraints = handleActions<MediaConstraintsStore, VideoConstraints>({
    [actionTypes.EDIT_USER_MEDIA_CONSTRAINTS]: (state, action) => ({
        ...state,
        video: action.payload
    }),
    [actionTypes.CLEAR_USER_MEDIA_CONSTRAINTS]: (state, action) => ({
        ...state,
        video: {}
    })
}, INITIAL_STATE);

export default mediaConstraints;
