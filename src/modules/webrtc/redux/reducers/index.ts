import { combineReducers } from 'redux';
import mediaConstraints from './media-constraints';
import sdpBandwidth from './sdp-bandwidth';

const rootReducer = combineReducers({
    mediaConstraints,
    sdpBandwidth
});

export default rootReducer;
