export interface VideoConstraints {
    width?: ConstrainULongRange,
    height?: ConstrainULongRange,
    frameRate?: ConstrainDoubleRange
}

export interface RootStore {
    mediaConstraints: MediaConstraintsStore;
    sdpBandwidth: BandwidthStore;
};

export interface BandwidthStore {
    bandwidth: number;
};

export interface MediaConstraintsStore {
    video: VideoConstraints;
};
