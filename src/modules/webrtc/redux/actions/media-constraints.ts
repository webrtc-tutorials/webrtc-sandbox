import { createAction } from 'redux-actions';
import * as actionTypes from '../constants/action-types';
import { VideoConstraints } from '../typings';

export const editUserMediaConstraints = createAction<VideoConstraints>(actionTypes.EDIT_USER_MEDIA_CONSTRAINTS);
export const clearUserMediaConstraints = createAction<void>(actionTypes.CLEAR_USER_MEDIA_CONSTRAINTS);
