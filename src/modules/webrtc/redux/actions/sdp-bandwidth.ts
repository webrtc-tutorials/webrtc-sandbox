import { createAction } from 'redux-actions';
import * as actionTypes from '../constants/action-types';

export const editSdpBandwidth = createAction<number>(actionTypes.EDIT_SDP_BANDWIDTH);
