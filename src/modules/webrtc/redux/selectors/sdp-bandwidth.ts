import { RootStore } from '../typings';

export const getSdpBandwidth = (store: RootStore) => store.sdpBandwidth.bandwidth;
