function convertVideoProps(props: MediaTrackConstraints): MediaStreamConstraints {
    const { width, height, frameRate } = props;

    return {
        audio: true,
        video: {
            facingMode: "user",
            width,
            height,
            frameRate
        }
    };
}

export function fetchLocalMedia(props: MediaTrackConstraints, onSuccess: (stream: MediaStream) => void, onError: (error: any) => void) {
    const constraints: MediaStreamConstraints = convertVideoProps(props);
    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess).catch(onError);
}

export function updateBandwidthRestriction(sdp: string, bandwidth: number, browser: string) {
    let modifier = 'AS';

    if (browser === 'firefox') {
        bandwidth = (bandwidth >>> 0) * 1000;
        modifier = 'TIAS';
    }

    if (sdp.indexOf('b=' + modifier + ':') === -1) {
        // insert b= after c= line.
        sdp = sdp.replace(/c=IN (.*)\r\n/, 'c=IN $1\r\nb=' + modifier + ':' + bandwidth + '\r\n');
    } else {
        sdp = sdp.replace(new RegExp('b=' + modifier + ':.*\r\n'), 'b=' + modifier + ':' + bandwidth + '\r\n');
    }

    return sdp;
}

export function removeBandwidthRestriction(sdp: string) {
    return sdp.replace(/b=AS:.*\r\n/, '').replace(/b=TIAS:.*\r\n/, '');
}
