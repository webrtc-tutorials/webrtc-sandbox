import React, { RefObject } from 'react';
import { Divider } from 'antd';
import { connect } from 'react-redux';
import adapter from 'webrtc-adapter';

import { LocalVideo, RemoteVideo } from './video-call-flow.styled';
import PageHeader from '../../../../components/common/PageHeader';
import JoinChannel from '../join-channel';

import { MessageType, MessageSubType } from '../../typings/message';
import { VideoConstraints, RootStore } from '../../redux/typings';
import { getVideoConstraints } from '../../redux/selectors/media-constraints';
import { getSdpBandwidth } from '../../redux/selectors/sdp-bandwidth';

import { updateBandwidthRestriction, removeBandwidthRestriction } from '../../util/webrtc-utils';

interface Props {
    socket: SocketIOClient.Socket;
    videoConstraints: VideoConstraints;
    bandwidth: number;
}

interface State {
    channel: string;
    constraints?: VideoConstraints;
    bandwidth?: number;
    isJoined: boolean;
    isChannelReady: boolean;
    isInitiator: boolean;
    isStarted: boolean;
}

class VideoCallFlow extends React.Component<Props, State> {

    props: Props;

    readonly state: State = {
        channel: '',
        isJoined: false,
        isChannelReady: false,
        isInitiator: false,
        isStarted: false
    };

    private localVideoRef = React.createRef<HTMLVideoElement>();
    private remoteVideoRef = React.createRef<HTMLVideoElement>();

    private pcConfig: RTCConfiguration = {
        iceServers: [
            { urls: ['stun:stun.l.google.com:19302'] }
        ]
    };

    private pc: RTCPeerConnection;
    private localStream: MediaStream;
    //private remoteStream: MediaStream;

    // getUserMedia() handlers...

    private attachMediaStream = (elementRef: RefObject<HTMLVideoElement>, stream: MediaStream, muted: boolean = false) => {
        const videoTag = elementRef.current;

        if (videoTag) {
            videoTag.srcObject = stream;
            videoTag.muted = muted;
        }
    };

    private handleUserMedia = (stream: MediaStream) => {
        this.localStream = stream;
        this.attachMediaStream(this.localVideoRef, stream, true);
        console.log('Adding local stream.');
        this.sendMessage(MessageType.gotUserMedia);
    };

    private handleUserMediaError = (error) => {
        console.log('navigator.getUserMedia error: ', error);
    };

    // Send message to the other peer via the signaling server
    private sendMessage = (message) => {
        const { socket } = this.props;
        const { channel } = this.state;

        console.log(`Sending message: ${JSON.stringify(message)}`);
        socket.emit(MessageType.message, { channel, message });
    };

    // Channel negotiation trigger function
    private checkAndStart = () => {
        const { isStarted, isChannelReady, isInitiator } = this.state;

        if (!isStarted && typeof this.localStream != 'undefined' && isChannelReady) {
            this.createPeerConnection();
            this.setState({ isStarted: true });

            if (isInitiator) {
                this.doCall();
            }
        }
    };

    // PeerConnection management...
    private createPeerConnection = () => {
        console.log('createPeerConnection()');

        try {
            this.pc = new RTCPeerConnection(this.pcConfig);

            if (!this.localStream) return;
            this.localStream.getTracks().forEach(track => this.pc.addTrack(track, this.localStream));

            this.pc.onicecandidate = this.handleIceCandidate;

            console.log('Created RTCPeerConnnection with:');
            console.log(`---> config: '${JSON.stringify(this.pcConfig)}'`);
        } catch (ex) {
            console.log(`Failed to create PeerConnection, exception: ${ex.message}`);
            alert('Cannot create RTCPeerConnection object.');
            return;
        }

        if (!this.pc) return;
        // this.pc.onaddstream = this.handleRemoteStreamAdded;
        // this.pc.onremovestream = this.handleRemoteStreamRemoved;

        this.pc.ontrack = (event: RTCTrackEvent) => {
            this.handleRemoteStreamAdded(event.streams[0]);
        };

    };

    // ICE candidates management
    private handleIceCandidate = (event) => {
        console.log('handleIceCandidate event: ', event);

        if (event.candidate) {
            this.sendMessage({
                type: 'candidate',
                label: event.candidate.sdpMLineIndex,
                id: event.candidate.sdpMid,
                candidate: event.candidate.candidate
            });
        } else {
            console.log('End of candidates.');
        }
    };

    // Create Offer
    private doCall = () => {
        console.log('Creating Offer...');

        this.pc.createOffer()
            .then(this.setLocalAndSendMessage)
            .catch(this.onSignalingError);
    };

    // Create Answer
    private doAnswer = () => {
        console.log('Sending answer to peer.');

        this.pc.createAnswer()
            .then(this.setLocalAndSendMessage)
            .catch(this.onSignalingError)
    };

    // Success handler for both createOffer() and createAnswer()
    private setLocalAndSendMessage = (desc: RTCSessionDescriptionInit) => {
        const { bandwidth } = this.props;
        const { browser } = adapter.browserDetails;
        const sDesc = desc;

        if (!sDesc.sdp) return;

        console.log(`Patching SDP: Updating Bandwidth with new value: ${bandwidth}`);

        const description = {
            type: sDesc.type,
            sdp: (bandwidth === 0)
                ? removeBandwidthRestriction(sDesc.sdp)
                : updateBandwidthRestriction(sDesc.sdp, bandwidth, browser)
        };

        console.log('Applying bandwidth restriction to setRemoteDescription:');
        console.log(`${description.sdp}`);

        this.pc.setLocalDescription(description)
            .then(() => {
                this.sendMessage(this.pc.localDescription);
            });
    };

    // Signaling error handler
    private onSignalingError = (error) => {
        console.log(`Failed to create signaling message: ${error.name}`);
    };

    // Remote stream handlers...

    private handleRemoteStreamAdded = (stream) => {
        console.log('Remote stream added.');
        this.attachMediaStream(this.remoteVideoRef, stream);
    };

    private handleRemoteStreamRemoved = (stream) => {
        console.log(`Remote stream removed. Stream: ${stream}`);
    };

    // Clean-up functions...

    private hangup = () => {
        console.log('Hanging up.');
        this.stop();
        this.sendMessage(MessageType.bye);
    };

    private handleRemoteHangup = () => {
        console.log('Session terminated.');
        this.stop();
        this.setState({ isInitiator: false });
    };

    private stop = () => {
        this.setState({ isStarted: false });

        if (this.pc) {
            this.pc.close();
        }

        if (this.localStream) {
            this.localStream.getTracks().forEach(track => track.stop());
        }
    };

    // ----------------------------------------------------------------------------------- //
    //    UI LOGIC
    // ----------------------------------------------------------------------------------- //

    static getDerivedStateFromProps(nextProps: Props, prevState: State) {
        return {
            ...prevState,
            constraints: nextProps.videoConstraints,
            bandwidth: nextProps.bandwidth
        };
    }

    componentDidMount(): void {
        const { socket } = this.props;

        socket.on(MessageType.log, (payload: string) => {
            console.log(`Log: ${payload}`);
        });

        socket.on(MessageType.created, (payload: string) => {
            console.log(`Channel: '${payload}' has been created!`);
            console.log('This peer is the initiator...');

            this.setState({
                isJoined: true,
                channel: payload
            });

            const constraints = { audio: true, video: this.state.constraints };

            // Call getUserMedia()
            console.log(`Getting user media with constraints: ${JSON.stringify(constraints)}`);

            navigator.mediaDevices.getUserMedia(constraints)
                .then(this.handleUserMedia)
                .catch(this.handleUserMediaError);

            this.checkAndStart();
        });

        // Handle 'full' message
        socket.on(MessageType.full, (payload: string) => {
            console.log(`Channel '${payload}' is too crowded! Cannot allow you to enter, sorry.`);
        });

        // Handle 'remotePeerJoining' message
        socket.on(MessageType.remotePeerJoining, (payload: string) => {
            console.log(`Another peer made a request to join room '${payload}'`);
            console.log('You are the initiator!');

            this.setState({isInitiator: true});
        });

        // Handle 'joined' message
        socket.on(MessageType.joined, (payload: string) => {
            console.log(`Message from server: ${payload}`);

            const constraints = { audio: true, video: this.state.constraints };

            // Call getUserMedia()
            console.log(`Getting user media with constraints: ${JSON.stringify(constraints)}`);

            navigator.mediaDevices.getUserMedia(constraints)
                .then(this.handleUserMedia)
                .catch(this.handleUserMediaError);

            this.setState({
                isJoined: true,
                isChannelReady: true
            });
        });

        // Handle 'broadcast: joined' message
        socket.on(MessageType.broadcastJoined, (payload: string) => {
            console.log(`Broadcast message from server: ${payload}`);
            this.setState({ isChannelReady: true });
        });

        // Handle 'message' message
        socket.on(MessageType.message, (payload) => {
            const { isStarted, isInitiator } = this.state;

            console.log(`Got message from other peer: ${JSON.stringify(payload)}`);

            if (payload === MessageType.gotUserMedia) {
                this.checkAndStart();
            } else if (payload === MessageType.bye && isStarted) {
                this.handleRemoteHangup();
            } else if (payload.type === MessageSubType.offer) {
                if (!isInitiator && !isStarted) {
                    this.checkAndStart();
                }

                this.pc.setRemoteDescription(new RTCSessionDescription(payload));
                this.doAnswer();
            } else if (payload.type === MessageSubType.answer && isStarted) {
                this.pc.setRemoteDescription(new RTCSessionDescription(payload));
            } else if (payload.type === MessageSubType.candidate && isStarted) {
                const candidate = new RTCIceCandidate({
                    sdpMLineIndex: payload.label,
                    candidate: payload.candidate
                });

                this.pc.addIceCandidate(candidate);
            }
        });

        // Handle 'response' message
        socket.on(MessageType.response, (payload: string) => {
            console.log(`Got response from other peer: ${payload}`);
        });

        // Handle 'bye' message
        socket.on(MessageType.bye, () => {
            console.log('Got "Bye" from other peer! Going to disconnect...');
            console.log('Sending "Ack" to server');

            socket.emit(MessageType.ack);

            // Disconnect from server
            console.log('Going to disconnect...');

            this.handleRemoteHangup();
            socket.disconnect();

            this.setState({ 
                isJoined: false, 
                isChannelReady: false, 
                isInitiator: false, 
                isStarted: false 
            });
        });
    }

    componentWillUnmount() {
        this.hangup();
    }

    createOrJoin = (channel: string) => {
        const { socket } = this.props;

        if (!socket.connected) {
            socket.connect();
        }

        console.log(`Trying to create or join channel: '${channel}'`);

        this.setState({ channel });
        socket.emit(MessageType.createOrJoin, channel);
    };

    leave = () => {
        const { socket } = this.props;

        console.log('Sending "Bye" to server');

        socket.emit(MessageType.bye, this.state.channel);

        console.log('Going to disconnect...');

        this.hangup();
        socket.disconnect();

        this.setState({ 
            isJoined: false, 
            isChannelReady: false, 
            isInitiator: false, 
            isStarted: false 
        });
    };

    render() {
        const { isJoined } = this.state;

        return (
            <>
                <PageHeader>Video Call Flow</PageHeader>

                <JoinChannel isJoined={isJoined} onJoin={this.createOrJoin} onLeave={this.leave} />

                <Divider dashed />

                <div>
                    <LocalVideo ref={this.localVideoRef} autoPlay></LocalVideo>
                </div>
                <div>
                    <RemoteVideo ref={this.remoteVideoRef} autoPlay></RemoteVideo>
                </div>
            </>
        );
    }
}

const mapStateToProps = (store: RootStore) => {
    return {
        videoConstraints: getVideoConstraints(store),
        bandwidth: getSdpBandwidth(store) 
    };
};

export default connect(mapStateToProps)(VideoCallFlow);
