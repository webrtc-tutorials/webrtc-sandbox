import { Button as AntButton } from 'antd';
import styled from 'styled-components';

const LocalVideo = styled.video`
    margin: 5px;
    border: 1px solid lightgrey;
    height: 100px;
`;

const RemoteVideo = styled.video`
    margin: 5px;
    border: 1px solid lightgrey;
    height: 500px;
`;

const Button = styled(AntButton)`
    margin-right: 5px;
`;

export { 
    LocalVideo, 
    RemoteVideo, 
    Button
};
