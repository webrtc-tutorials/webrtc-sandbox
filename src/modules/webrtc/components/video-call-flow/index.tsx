import React from 'react';
import VideoCallFlow from './video-call-flow';
import SocketContext from '../../context/socket-context';

const VideoCallFlowPage = () => (
    <SocketContext.Consumer>
        {socket => <VideoCallFlow socket={socket} />}
    </SocketContext.Consumer>
);

export default VideoCallFlowPage;