import React from 'react';
import { connect } from 'react-redux';
import { Form, InputNumber, Input, Col } from "antd";

import modalForm from "../../../../components/hoc/modalForm";

import { editUserMediaConstraints } from '../../redux/actions/media-constraints';
import { getVideoConstraints } from '../../redux/selectors/media-constraints';
import { VideoConstraints, RootStore } from "../../redux/typings";

interface Props {
    model: VideoConstraints;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (formData: VideoConstraints) => void;
}

interface FormData {
    minWidth: number;
    maxWidth: number;
    minHeight: number;
    maxHeight: number;
    minFrameRate: number;
    maxFrameRate: number;
}

const FormComponent = modalForm((props) => {
    const { form, entity } = props;
    const { width, height, frameRate } = entity;

    const formData: FormData = {
        minWidth: (width) ? width.min : 0,
        maxWidth: (width) ? width.max : 0,
        minHeight: (height) ? height.min : 0,
        maxHeight: (height) ? height.max : 0,
        minFrameRate: (frameRate) ? frameRate.min : 0,
        maxFrameRate: (frameRate) ? frameRate.max : 0,
    }

    return (
        <Form>
            <Input.Group compact>
                <Col span={12}>
                    <Form.Item label="Width min">
                        {form.getFieldDecorator("minWidth", {
                            initialValue: formData.minWidth,
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={12}>
                    <Form.Item label="Width max">
                        {form.getFieldDecorator("maxWidth", {
                            initialValue: formData.maxWidth,
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>
            </Input.Group>

            <Input.Group compact>
                <Col span={12}>
                    <Form.Item label="Height min">
                        {form.getFieldDecorator("minHeight", {
                            initialValue: formData.minHeight,
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={12}>
                    <Form.Item label="Height max">
                        {form.getFieldDecorator("maxHeight", {
                            initialValue: formData.maxHeight,
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>
            </Input.Group>

            <Input.Group compact>
                <Col span={12}>
                    <Form.Item label="FRate min">
                        {form.getFieldDecorator("minFrameRate", {
                            initialValue: formData.minFrameRate,
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>

                <Col span={12}>
                    <Form.Item label="FRate max">
                        {form.getFieldDecorator("maxFrameRate", {
                            initialValue: formData.maxFrameRate,
                            rules: [{ required: true, message: "Required!" }],
                        })(
                            <InputNumber min={0} />
                        )}
                    </Form.Item>
                </Col>
            </Input.Group>
        </Form>
    )
});

class UserMediaSettings extends React.Component<Props> {

    static defaultProps = {
        isOpen: false
    };

    props: Props;

    submitForm = (data: FormData) => {
        const { onSubmit, onClose } = this.props;

        const videoProps: VideoConstraints = {
            width: {
                min: data.minWidth,
                max: data.maxWidth
            },
            height: {
                min: data.minHeight,
                max: data.maxHeight
            },
            frameRate: {
                min: data.minFrameRate,
                max: data.maxFrameRate
            }
        };

        onSubmit(videoProps);
        onClose();
    };

    render() {
        const { isOpen, onClose, model } = this.props;

        return (
            <FormComponent
                isOpen={isOpen}
                title="Get User Media Settings"
                entity={model}
                onCancel={onClose}
                onSubmit={this.submitForm}
            />
        );
    }
}

const mapStateToProps = (store: RootStore) => {
    return {
        model: getVideoConstraints(store)
    };
};


const mapDispatchToProps = (dispatch: any) => ({
    onSubmit: (constraints: VideoConstraints) => dispatch(editUserMediaConstraints(constraints))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserMediaSettings);
