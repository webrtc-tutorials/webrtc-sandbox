import React from 'react';
import { Divider } from 'antd';
import { connect } from 'react-redux';
import ReactJson from 'react-json-view'

import PageHeader from '../../../../components/common/PageHeader';
import { Button, ButtonGroup } from './settings.styled';
import UserMediaSettings from './user-media-settings';
import EditBandwidth from './edit-bandwidth';

import { VideoConstraints, RootStore } from '../../redux/typings';
import { clearUserMediaConstraints, editUserMediaConstraints } from '../../redux/actions/media-constraints';
import { getVideoConstraints } from '../../redux/selectors/media-constraints';
import { getSdpBandwidth } from '../../redux/selectors/sdp-bandwidth';

import { highVideoProps, mediumVideoProps, lowVideoProps } from '../../typings/media';

interface Props {
    videoConstraints: VideoConstraints;
    bandwidth: number;
    onClearMedia: () => void;
    onUpdateQuality: (video: VideoConstraints) => void;
}

interface State {
    isEditSettings: boolean;
    isEditBandwidth: boolean;
}

class Settings extends React.Component<Props, State> { 

    props: Props;

    readonly state: State = {
        isEditSettings: false,
        isEditBandwidth: false
    };
    
    openSettingsDialog = () => {
        this.setState({isEditSettings: true});
    };

    closeSettingsDialog = () => {
        this.setState({isEditSettings: false});
    };

    openBandwidthDialog = () => {
        this.setState({isEditBandwidth: true});
    };

    closeBandwidthDialog = () => {
        this.setState({isEditBandwidth: false});
    };

    clearMedia = () => {
        this.props.onClearMedia();
    };

    setLowQuality = () => {
        this.props.onUpdateQuality(lowVideoProps);
    };

    setMediumQuality = () => {
        this.props.onUpdateQuality(mediumVideoProps);
    };

    setHighQuality = () => {
        this.props.onUpdateQuality(highVideoProps);
    };

    render() { 
        const { videoConstraints, bandwidth } = this.props;
        const { isEditSettings, isEditBandwidth } = this.state;
        const constraints = { audio: true, video: videoConstraints };

        const headerActions = (
            <>
                <Button type="primary" onClick={this.clearMedia}>Clear Media</Button>
                <Button type="primary" onClick={this.openSettingsDialog}>Settings</Button>
                <Button type="primary" onClick={this.openBandwidthDialog}>Bandwidth</Button>
            </>
        );

        return (
            <>
                <PageHeader actions={headerActions}>
                    Settings
                </PageHeader>

                <UserMediaSettings isOpen={isEditSettings} onClose={this.closeSettingsDialog} />
                <EditBandwidth isOpen={isEditBandwidth} onClose={this.closeBandwidthDialog} />

                <ButtonGroup>
                    <Button type="primary" onClick={this.setLowQuality}>Low</Button>
                    <Button type="primary" onClick={this.setMediumQuality}>Medium</Button>
                    <Button type="primary" onClick={this.setHighQuality}>High</Button>
                </ButtonGroup>

                <Divider dashed />

                <div>
                    <div>Video constraints:</div>
                    <ReactJson name="constraints" src={constraints} enableClipboard={false}></ReactJson>
                </div>

                <Divider dashed />

                <div>
                    Bandwidth: {bandwidth} kbps 
                </div>
            </>
        );
    }
}

const mapStateToProps = (store: RootStore) => {
    return {
        videoConstraints: getVideoConstraints(store),
        bandwidth: getSdpBandwidth(store) 
    };
};

const mapDispatchToProps = (dispatch: any) => ({
    onClearMedia: () => dispatch(clearUserMediaConstraints()),
    onUpdateQuality: (video: VideoConstraints) => dispatch(editUserMediaConstraints(video))
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
