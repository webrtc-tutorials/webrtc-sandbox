import { Button as AntButton } from 'antd';
import styled from 'styled-components';

export const ButtonGroup = styled.div`
    padding-top: 1em;
`;

export const Button = styled(AntButton)`
    margin-right: 5px;
`;
