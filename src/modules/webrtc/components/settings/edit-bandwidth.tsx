import React from 'react';
import { connect } from 'react-redux';
import { Form, Radio } from "antd";

import modalForm from "../../../../components/hoc/modalForm";

import { editSdpBandwidth } from '../../redux/actions/sdp-bandwidth';
import { getSdpBandwidth } from '../../redux/selectors/sdp-bandwidth';
import { RootStore } from "../../redux/typings";

interface Props {
    model: number;
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (formData: number) => void;
}

interface FormData {
    bandwidth: number;
}

const FormComponent = modalForm((props) => {
    const { form, entity } = props;

    const formData: FormData = {
        bandwidth: entity
    };

    return (
        <Form>
            <Form.Item label="Bandwidth">
                {form.getFieldDecorator('bandwidth', {
                    initialValue: formData.bandwidth
                })(
                    <Radio.Group>
                        <Radio value={0}>Unlimited</Radio>
                        <Radio value={2000}>2000 kbps</Radio>
                        <Radio value={1000}>1000 kbps</Radio>
                        <Radio value={500}>500 kbps</Radio>
                        <Radio value={250}>250 kbps</Radio>
                        <Radio value={125}>125 kbps</Radio>
                    </Radio.Group>,
                )}
            </Form.Item>
        </Form>
    );
});

class EditBandwidth extends React.Component<Props> {
    
    static defaultProps = {
        isOpen: false
    };

    props: Props;

    submitForm = (data: FormData) => {
        const { onSubmit, onClose } = this.props;

        onSubmit(data.bandwidth);
        onClose();
    };

    render() {
        const { isOpen, onClose, model } = this.props;

        return (
            <FormComponent
                isOpen={isOpen}
                title="Edit SDP Bandwidth"
                entity={model}
                onCancel={onClose}
                onSubmit={this.submitForm}
            />
        );
    }
}

const mapStateToProps = (store: RootStore) => {
    return {
        model: getSdpBandwidth(store)
    };
};


const mapDispatchToProps = (dispatch: any) => ({
    onSubmit: (bandwidth: number) => dispatch(editSdpBandwidth(bandwidth))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditBandwidth);
