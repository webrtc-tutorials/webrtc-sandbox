import React from 'react';
import Signaling from './signaling';
import SocketContext from '../../context/socket-context';

const SignalingPage = () => (
    <SocketContext.Consumer>
        {socket => <Signaling socket={socket} />}
    </SocketContext.Consumer>
);

export default SignalingPage;
