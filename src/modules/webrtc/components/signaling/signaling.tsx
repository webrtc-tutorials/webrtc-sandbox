import React from 'react';
import {Divider, Card} from 'antd';

import PageHeader from '../../../../components/common/PageHeader';
import JoinChannel from '../join-channel';

import { MessageType } from '../../typings/message';

interface Props {
    socket: SocketIOClient.Socket;
}

interface State {
    channel: string;
    isJoined: boolean;
    signalingMessages: Array<[number, string]>;
}

class Signaling extends React.Component<Props, State> {

    props: Props;

    readonly state: State = {
        channel: '',
        isJoined: false,
        signalingMessages: []
    };

    private addSignalingMessage = (message: string) => {
        const timestamp = (performance.now() / 1000).toFixed(3);
        const key = this.state.signalingMessages.length;

        this.setState({
            signalingMessages: [
                ...this.state.signalingMessages,
                [key, `Time: ${timestamp} --> ${message}`]
            ]
        });
    };

    componentDidMount(): void {
        const {socket} = this.props;

        socket.on(MessageType.log, (payload: string) => {
            console.log(payload);
        });

        socket.on(MessageType.created, (payload: string) => {
            console.log(`Channel: '${payload}' has been created!`);
            console.log('This peer is the initiator...');

            this.addSignalingMessage(`Channel ${payload} has been created!`);
            this.addSignalingMessage('This peer is the initiator...');

            this.setState({isJoined: true});
        });

        // Handle 'full' message
        socket.on(MessageType.full, (payload: string) => {
            console.log(`Channel '${payload}' is too crowded! Cannot allow you to enter, sorry.`);
            this.addSignalingMessage(`Channel '${payload}' is too crowded! Cannot allow you to enter, sorry.`);
        });

        // Handle 'remotePeerJoining' message
        socket.on(MessageType.remotePeerJoining, (payload: string) => {
            console.log(`Request to join '${payload}'`);
            console.log('You are the initiator!');
            this.addSignalingMessage(`Message from server: request to join channel '${payload}'`);
        });

        // Handle 'joined' message
        socket.on(MessageType.joined, (payload: string) => {
            console.log(`Message from server: ${payload}`);
            this.addSignalingMessage('Message from server:');
            this.addSignalingMessage(payload);

            this.setState({isJoined: true});
        });

        // Handle 'broadcast: joined' message
        socket.on(MessageType.broadcastJoined, (payload: string) => {
            console.log(`Broadcast message from server: ${payload}`);
            this.addSignalingMessage('Broadcast message from server:');
            this.addSignalingMessage(payload);
        });

        // Handle 'message' message
        socket.on(MessageType.message, (payload: string) => {
            console.log(`Got message from other peer: ${payload}`);
        });

        // Handle 'response' message
        socket.on(MessageType.response, (payload: string) => {
            console.log(`Got response from other peer: ${payload}`);
        });

        // Handle 'bye' message
        socket.on(MessageType.bye, () => {
            console.log('Got "Bye" from other peer! Going to disconnect...');
            this.addSignalingMessage('Got "Bye" from other peer!');
            console.log('Sending "Ack" to server');
            this.addSignalingMessage('Sending "Ack" to server');

            socket.emit(MessageType.ack);

            // Disconnect from server
            console.log('Going to disconnect...');
            this.addSignalingMessage('Going to disconnect...');

            socket.disconnect();

            this.setState({isJoined: false});
        });
    }

    createOrJoin = (channel: string) => {
        const {socket} = this.props;

        console.log(`Trying to create or join channel: '${channel}'`);
        console.log(`Socket: connected: ${socket.connected}`);

        this.setState({channel});
        socket.emit(MessageType.createOrJoin, channel);
    };

    leave = () => {
        const {socket} = this.props;

        console.log('Sending "Bye" to server');
        this.addSignalingMessage('Sending "Bye" to server...');

        socket.emit(MessageType.bye, this.state.channel);

        console.log('Going to disconnect...');
        this.addSignalingMessage('Going to disconnect...');

        socket.disconnect();

        this.setState({isJoined: false});
    };

    render() {
        const {isJoined, signalingMessages} = this.state;

        return (
            <>
                <PageHeader>
                    Signaling
                </PageHeader>

                <JoinChannel isJoined={isJoined} onJoin={this.createOrJoin} onLeave={this.leave}/>

                <Divider dashed />

                <Card title="Signaling messages" size="small" bodyStyle={{height: 300, overflowY: "scroll"}}>
                    {signalingMessages && signalingMessages.map(([key, value]) =>
                        <p key={key}>{value}</p>
                    )}
                </Card>
            </>
        );
    }
}

export default Signaling;
