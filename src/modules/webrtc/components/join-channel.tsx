import React from 'react';
import { Form, Input, Button as AntButton } from 'antd';
import { FormComponentProps } from 'antd/es/form';
import styled from 'styled-components';

interface Props extends FormComponentProps {
    isJoined: boolean;
    onJoin: (channel: string) => void;
    onLeave: () => void;
}

const Button = styled(AntButton)`
    margin: 0 .5em;
`;

class JoinChannelForm extends React.Component<Props> {

    props: Props;

    handleSubmit = (event) => {
        event.preventDefault();

        const {form, onJoin } = this.props;

        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            onJoin(values.channel);
            form.resetFields();
        });
    };

    render() {
        const { form, onLeave, isJoined } = this.props;

        return (
            <>
                <Form layout="inline" onSubmit={this.handleSubmit}>
                    <Form.Item label="Join channel">
                        {form.getFieldDecorator("channel", {
                            rules: [{ required: true, message: "Channel name is required!" }],
                        })(
                            <Input placeholder="Channel" disabled={isJoined} style={{width: "300px"}} />
                        )}
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" disabled={isJoined}>Join</Button>
                        <Button onClick={onLeave} disabled={!isJoined}>Leave</Button>
                    </Form.Item>
                </Form>
            </>
        );
    }
}

const JoinChannel = Form.create<Props>()(JoinChannelForm);

export default JoinChannel;
