import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import io from 'socket.io-client';

import { Settings } from './components/settings';
import Signaling from './components/signaling';
import VideoCallFlow from './components/video-call-flow';

import SocketContext from './context/socket-context';

import * as routes from '../../routes';
import store from './store';
import {Provider} from "react-redux";

const socket = io.connect('https://poc.server.com');

const Video = (props: { match: any }) => (
    <SocketContext.Provider value={socket}>
        <Provider store={store}>
            <Switch>
                <Route
                    exact={true}
                    path={`${props.match.url}/`}
                    render={() =>
                        <Redirect to={`${props.match.url}/${routes.SIGNALING}`} />
                    }
                />
                <Route path={`${props.match.url}/${routes.SETTINGS}`} component={Settings} />
                <Route path={`${props.match.url}/${routes.SIGNALING}`} component={Signaling} />
                <Route path={`${props.match.url}/${routes.VIDEO_CALL_FLOW}`} component={VideoCallFlow} />
            </Switch>
        </Provider>
    </SocketContext.Provider>
);

export default Video;