// top items
export const DEMOS = 'demos';
export const WEBRTC = 'webrtc';
export const JANUS = 'janus';

// demos group
export const USER_MEDIA_CONSTRAINTS = 'user-media-constraints';
export const USER_MEDIA_BANDWIDTH = 'user-media-bandwidth';

// webrtc group
export const SETTINGS = 'settings';
export const SIGNALING = 'signaling';
export const VIDEO_CALL_FLOW = 'video-call-flow';

// janus group
export const ROOMS_LIST = 'rooms-list';
export const VIDEO_ROOM = 'video-room';
